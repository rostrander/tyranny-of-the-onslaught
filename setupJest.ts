import {AddableResult} from "./src/backend/addable_result";
import CustomMatcherResult = jest.CustomMatcherResult;
import {CommandResult} from "./src/backend/commands";

expect.extend({
    toHaveValue(addable: AddableResult, value: number): CustomMatcherResult {
        if (typeof value !== 'number') {
            throw new Error('expected value to be a number');
        }

        if (addable['value'] === undefined) {
            throw new Error('expected received to have a .value field');
        }

        let pass = addable.value === value;
        let message = () => `Expected AddableResult (value: ${addable.value}) to have value ${value}`

        return {pass, message}
    },

    toHaveSucceeded(result: CommandResult): CustomMatcherResult {
        if (!result) {
            return {
                pass: false,
                message: () => `Expected successful command, got ${result}`
            }
        }

        let pass = result.success;
        let un = pass ? 'un' : '';
        let message = () => `Expected ${un}successful command, got ${result.success}`;
        return {pass, message}
    }
});
