// GUIDE FOR HOW TO SET UP VUE IN PRE-EXISTING CODEBASE:
// https://v3.vuejs.org/guide/installation.html#npm

import "../lib/bootstrap.min.css";
import "bootstrap";
import { createApp } from 'vue'
import App from './App.vue'

createApp(App).mount('#app')