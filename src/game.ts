import * as ROT from 'rot-js'
import {StateDriver} from "./frontend/statedriver";
import {PlayingScreen} from "./frontend/playing_screen";
import {Character} from "./backend/character";

export function startup(): Character {
    console.log("Loaded!");

    let driver = new StateDriver();
    let title = new PlayingScreen(driver);
    driver.push(title);

    // For debugging
    (window as any)['driver'] = driver;
    return title.player
}