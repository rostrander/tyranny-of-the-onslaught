import * as ROT from 'rot-js'

import {State, StateDriver} from "./statedriver";
import {XY} from "../backend/common";
import {Action, actionToDirection} from "./keybinds";
import {CommandFactory, UseAbilityCommand} from "../backend/commands";
import {GameMaster} from "../backend/gamemaster";
import {Character} from "../backend/character";
import {DebugGameFactory, NewGameFactory} from "../backend/newgame";
import {PlayerBehavior} from "../backend/behavior";
import {PubSubMessage} from "../backend/pubsub";
import {TargetingScreen} from "./targeting_screen";
import {MapDrawer} from "./common";
import {Tile} from "../backend/world";
import {BasicRangedAbility} from "../backend/ability";
import {AbilityScreen} from "./ability_screen";
import {InventoryState} from "./inventory_screen";

export class PlayingScreen extends State implements MapDrawer {

    protected tick: number;
    public gm: GameMaster;
    public player: Character;
    protected pb: PlayerBehavior;

    protected cmd: CommandFactory;
    protected messages: string[];

    constructor(driver: StateDriver) {
        super(driver);
        this.tick = 0;

        //let factory = new NewGameFactory();
        let factory = new DebugGameFactory();
        this.gm = factory.newGame();
        this.player = this.gm.player;
        this.player.name = "Player McGee";
        this.player.messages.sub((msg) => this.gotMessage(msg));
        this.pb = this.player.behavior as PlayerBehavior;

        this.cmd = new CommandFactory(this.gm, this.player);
        this.messages = [];
    }

    public frame() {
        this.tick += 1;
        this.gm.turn();
        this.driver.refresh();
        super.frame();
    }

    public render(display: ROT.Display) {
        this.drawEntireMap(display, this.player)
        display.drawText(1,  1, `Tick: ${this.tick}`);
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        let dir = actionToDirection(action);
        if(dir) {
            let dirs = [dir]
            if(ev.shiftKey) {
                for(let i=0; i < 4; i++) {
                    dirs.push(dir);
                }
            }
            let moves = this.cmd.superMove(...dirs);
            if(moves && moves.length > 0) {
                this.pb.addCommand(...moves);
            }
        }

        switch(action) {
            case Action.SkipTurn:
                this.pb.suggestCommand(this.cmd.sitThere());
                break;
            case Action.DefaultRangedScreen:
                this.target();
                break;
            case Action.AbilityScreen:
                this.driver.push(new AbilityScreen(this.driver, this.gm));
                break;
            case Action.InventoryScreen:
                this.driver.push(new InventoryState(this.driver, this.pb));
                break;
        }
    }

    public mapCoordsToScreen(mapCoords: XY, center: XY): XY {
        let halfWidth = Math.floor(this.driver.width / 2);
        let halfHeight = Math.floor(this.driver.height / 2);

        let dx = mapCoords.x - center.x;
        let dy = mapCoords.y - center.y;
        return {
            x: halfWidth + dx,
            y: halfHeight + dy,
        };
    }

    public screenCoordsToMap(screenCoords: XY, center: XY): XY {
        let halfWidth = Math.floor(this.driver.width / 2);
        let halfHeight = Math.floor(this.driver.height / 2);
        let left = center.x - halfWidth;
        let top = center.y - halfHeight;

        return {
            x: left + screenCoords.x,
            y: top + screenCoords.y,
        }
    }

    public drawEntireMap(display: ROT.Display, center: XY) {
        let map = this.gm.map;
        for (let x = 0; x < map.width; x++) {
            for (let y = 0; y < map.height; y++) {
                this.drawTileAt(display, {x, y}, center);
            }
        }
    }

    public async target() {
        let ability = this.player.defaultRangedAbility;
        let ts = new TargetingScreen(this.driver, this, ability);
        await this.driver.push(ts);
        if(ts.targetLoc) {
            let cmd = new UseAbilityCommand(this.gm, this.player, ability, ts.targetLoc);
            this.pb.addCommand(cmd);
        }
    }

    public drawTileAt(display: ROT.Display, tileLoc: XY, center: XY, bg="#000"): boolean {
        let m = this.mapCoordsToScreen(tileLoc, center);
        let map = this.gm.map;
        if (map.locationInBounds(tileLoc)) {
            let tile = map.tileAt(tileLoc);
            let visible = this.player.canSee(tile);
            let drawWithoutChar = visible === 0 && tile.viewed;
            if(!visible && !drawWithoutChar) return false;

            let fgcolor = tile.fgcolor(visible);
            let color = bg === "#000" ? ROT.Color.toHex(fgcolor) : "#000";
            display.draw(m.x, m.y, tile.displayedChar(!drawWithoutChar),
                color,
                bg);
            return true;
        }
    }

    public tileAt(loc: XY): Tile {
        return this.gm.map.tileAt(loc);
    }

    public gotMessage(msg: PubSubMessage) {
        if(msg.text) {
            this.messages.push(msg.text);
        }
    }
}