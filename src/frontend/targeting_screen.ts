import * as ROT from 'rot-js';
import {State, StateDriver} from "./statedriver";
import {XY} from "../backend/common";
import {Character} from "../backend/character";
import {GameMaster} from "../backend/gamemaster";
import {Action, actionToDirection} from "./keybinds";
import {MapDrawer} from "./common";
import {Ability} from "../backend/ability";

export class TargetingScreen extends State {
    public player: Character;
    public targetLoc: XY;

    protected gm: GameMaster;

    public static lastTarget: Character;

    constructor(
        driver: StateDriver,
        protected mapDraw: MapDrawer,
        protected ability: Ability,
    ) {
        super(driver);
        this.gm = mapDraw.gm;
        this.player = this.gm.player;
        this.determineDefaultTargetLoc();
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        super.handleInput(action, ev);
        let dir = actionToDirection(action);
        let mult = ev.shiftKey ? 5 : 1;
        if(dir) {
            let newTarget = {
                x: this.targetLoc.x + dir.x * mult,
                y: this.targetLoc.y + dir.y * mult
            };
            if(!this.mapDraw.tileAt(newTarget).isNullTile()) {
                this.targetLoc = newTarget;
            }
            return;
        }

        switch(action) {
            case Action.Cancel:
                this.cancel();
                break;
            case Action.Confirm:
            case Action.DefaultRangedScreen:
                if(!this.ability.canCenterAt(this.targetLoc)) return this.cancel();
                let who = this.gm.map.tileAt(this.targetLoc);
                TargetingScreen.lastTarget = who.character;
                this.driver.pop();
                break;
        }
    }

    protected cancel() {
        this.targetLoc = null;
        this.driver.pop();
    }

    protected determineDefaultTargetLoc() {
        if(
            TargetingScreen.lastTarget?.alive &&
            this.ability.canCenterAt(TargetingScreen.lastTarget)
        ) {
            this.targetLoc = TargetingScreen.lastTarget;
        } else {
            let closest = this.gm.closestEnemyTo(this.player);
            if (closest && this. ability.canCenterAt(closest)) {
                this.targetLoc = closest;
                return;
            }
            this.targetLoc = this.player;
        }
    }

    public mouseMove(loc: XY) {
        super.mouseMove(loc);
        if(loc.x < 0 || loc.y < 0) {
            this.targetLoc = this.player.loc;
        } else {
            this.targetLoc = this.mapDraw.screenCoordsToMap(loc, this.player.loc);
        }
        this.driver.refresh();
    }

    public mouseUp(loc: XY) {
        this.driver.pop();
    }

    public render(display: ROT.Display) {
        this.mapDraw.drawEntireMap(display, this.player.loc);
        let verb = "On this spot is: ";
        display.drawText(0, 1, `${verb} ${this.tileDescription(this.targetLoc)}`);

        let canTarget = this.ability.canCenterAt(this.targetLoc);
        let bg = canTarget ? "#888" : "#822";
        this.mapDraw.drawTileAt(display, this.targetLoc, this.player.loc, bg);

        super.render(display);
    }

    protected tileDescription(loc: XY): string {
        let tile = this.mapDraw.tileAt(loc);

        if(!this.player.canSee(tile)) {
            return "Darkness";
        }

        if(tile.character) {
            return `A ${tile.character.name}`;
        }

        switch(tile.glyph) {
            case '.':
                return "A stone floor";
            case '#':
                return "A stone wall";
            default:
                return "A bug!";
        }
    }
}