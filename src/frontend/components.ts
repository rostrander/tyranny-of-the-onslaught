import * as ROT from 'rot-js'

import {Action} from "./keybinds";
import {Component, State} from "./statedriver";
import {XY} from "../backend/common";

class ListEntry {
    public disabled: boolean;

    constructor(
        public data: any,
        protected _label?: string,
        public nullFallback = "???",
    ) {
        this.disabled = false;
    }

    public get label() {
        return this._label ||
            this.data?.name ||
            this.data?.toString() ||
            this.nullFallback;
    }
}

export class ListComponent extends Component {
    public selectedIndex: number;
    public maxDisplayedItems: number;
    public disabled: boolean;

    protected topOffset: number;
    public items: ListEntry[];

    constructor(parent: State, items: any[], where: XY) {
        super(parent, where);
        this.selectedIndex = 0;
        this.maxDisplayedItems = 20;
        this.topOffset = 0;
        this.items = items.map(i => new ListEntry(i));
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        if(this.disabled) return;
        if (action == Action.North) {
            this.selectIndex(this.selectedIndex-1);
        } else if (action == Action.South) {
            this.selectIndex(this.selectedIndex + 1);
        }
    }

    public render(display: ROT.Display) {
        let displayed = 0;
        for(let offset=0; offset < this.items.length && displayed < this.maxDisplayedItems; offset++) {
            let item = this.items[offset + this.topOffset];
            let textPrefix = "";
            let fg = item.disabled ? "#888" : "#FFF"
            let bg = "";
            if(!this.disabled && offset + this.topOffset === this.selectedIndex) {
                bg = fg;
                fg = "#000";
            }
            textPrefix = `%b{${bg}}%c{${fg}}`;
            display.drawText(
                this.loc.x,
                this.loc.y+offset,
                textPrefix+item.label
            );
            displayed+=1;
        }
    }

    public get selectedItem(): ListEntry {
        return this.items[this.selectedIndex];
    }

    public refresh() {
        this.selectIndex(this.selectedIndex);
    }

    protected selectIndex(newIndex?: number) {
        let len = this.items.length;
        if(newIndex === undefined) newIndex = 0;
        let sel = ROT.Util.mod(newIndex, len);
        this.selectedIndex = sel;

        if(len <= this.maxDisplayedItems) return;
        let center = Math.floor(this.maxDisplayedItems / 2);
        if(sel <= center) {
            this.topOffset = 0;
            return;
        } else if(sel >= len - center) {
            this.topOffset = len - this.maxDisplayedItems;
        } else {
            this.topOffset = sel - center;
        }
    }
}

type RibbonCallback = ()=>void;
interface RibbonInfo {
    action: Action;
    text: string;
    disabled?: boolean;
    callback: RibbonCallback;
}
type RibbonTuple = [Action, string, RibbonCallback?];

export class RibbonComponent extends Component {
    protected infoByAction: {[index:number] : RibbonInfo}
    protected cmds: RibbonInfo[];

    constructor(parent: State, cmds: RibbonTuple[], where= {x:0, y: 0}) {
        super(parent, where);
        this.infoByAction = {};
        this.cmds = [];
        for(let cmd of cmds) {
            let callback = cmd[2];
            if(callback) callback = callback.bind(parent);
            let info = {
                action: cmd[0],
                text: cmd[1],
                callback: callback,
            }
            this.cmds.push(info);
            this.infoByAction[info.action] = info;
        }
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        let info = this.infoByAction[action];
        if(info && info.callback && !info.disabled) {
            info.callback();
        }
    }

    protected getKey(action: Action) {
        return this.parent.driver.keybinds.actionToFirstKey(action);
    }

    public setEnabled(action: Action, enable = true) {
        this.infoByAction[action].disabled = !enable;
    }

    public render(display: ROT.Display) {
        let resultList = this.cmds.map((item) => {
            let prefix = item.disabled ? "%c{#888}" : "";
            let suffix = item.disabled ? "%c{}": "";
            return `${prefix}${this.getKey(item.action)}: ${item.text}${suffix}`;
        });
        let ribbon = resultList.join(" ");
        display.drawText(this.loc.x, this.loc.y, ribbon);
    }
}