import * as ROT from 'rot-js';
import {State, StateDriver} from "./statedriver";
import {ListComponent, RibbonComponent} from "./components";
import {Character} from "../backend/character";
import {GameMaster} from "../backend/gamemaster";
import {Action} from "./keybinds";
import {EquippableItem, Item} from "../backend/item";
import {EquipItemCommand, UnequipItemCommand} from "../backend/commands";
import {PlayerBehavior} from "../backend/behavior";

export class InventoryState extends State {

    protected inventoryList: ListComponent;
    protected equippedList: ListComponent;
    protected ribbon: RibbonComponent;
    protected char: Character;
    protected gm: GameMaster;

    constructor(driver: StateDriver, protected pb: PlayerBehavior) {
        super(driver);
        this.gm = pb.gm;
        this.char = pb.char;

        this.inventoryList = new ListComponent(this, this.char.inventory, {x: 1, y: 2});
        this.addComponent(this.inventoryList);

        let slots = [];
        for (let key in this.char.equipment) {
            slots.push(this.char.equipment[key]);
        }
        let halfWidth = this.driver.width / 2;
        this.equippedList = new ListComponent(this, slots, {x: halfWidth, y: 3});
        this.equippedList.disabled = true;
        for(let item of this.equippedList.items) {
            item.nullFallback = "Nothing";
        }
        this.addComponent(this.equippedList);

        this.ribbon = new RibbonComponent(this, [
            [Action.Cancel, 'quit', this.quit],
            [Action.Drop, 'drop'],
            [Action.Use, 'use'],
            [Action.Equip, 'equip', this.equipSelectedItem],
            [Action.Unequip, 'unequip', this.unequipSelectedItem],

            [Action.North, 'up'],
            [Action.South, 'down'],
            [Action.East, 'right', this.swapLists],
            [Action.West, 'left', this.swapLists],
        ]);
        this.addRibbon(this.ribbon);
    }

    protected quit() {
        this.driver.pop();
    }

    protected swapLists() {
        this.inventoryList.disabled = !this.inventoryList.disabled;
        this.equippedList.disabled = !this.equippedList.disabled;
    }

    protected get selectedItem(): Item {
        let activeList = this.inventoryList.disabled ?
            this.equippedList :
            this.inventoryList;
        return activeList.selectedItem?.data as Item;
    }

    public render(display: ROT.Display) {
        display.drawText(0, 0, "You are carrying:");

        let halfWidth = this.driver.width / 2;
        let y = 3;
        for(let slot in this.char.equipment) {
            display.drawText(halfWidth - 10, y, slot);
            y += 1;
        }

        let item = this.selectedItem;

        this.ribbon.setEnabled(Action.Use, !!item?.usable);
        this.ribbon.setEnabled(Action.Equip, !!item?.equippable);
        if(!this.equippedList.disabled) {
            this.ribbon.setEnabled(Action.Equip, false);  // already equipped
            this.ribbon.setEnabled(Action.Unequip, item != null);
        }

        super.render(display);

        if(item) {
            this.renderSelectedInfo(display);
        }

    }

    public renderSelectedInfo(display: ROT.Display) {
        let selectedItem = this.selectedItem;
        if(!selectedItem) return;
        let halfWidth = this.driver.width / 2;

        if(selectedItem.equippable) {
            let equ = <EquippableItem>selectedItem;

            let height = Object.keys(this.char.equipment).length + 4;
            let y = height;
            for(let key of equ.slots) {
                display.drawText(halfWidth, y, `Equipped in ${key} slot:`);
                let equipped = this.char.equipment[key];

                let name = equipped?.name || 'Nothing';
                let desc = equipped?.description() || '';
                display.drawText(halfWidth, y + 1, name);
                display.drawText(halfWidth, y+2, desc, halfWidth);
                y += height;
            }
        }
    }

    protected dropSelectedItem() {
        let item = this.selectedItem;
        if(item) {
            // TODO: Drop item command
            this.driver.pop();
        }
    }

    protected useSelectedItem() {
        // Not confused with below because ribbon will only fire callbacks
        // if enabled.
        // TODO: Use item command
        let item = this.selectedItem;
        if(!item || !item.usable) return false;
    }

    protected unequipSelectedItem() {
        let item = this.selectedItem;
        if(!item) return false;
        let slotIdx = this.equippedList.selectedIndex;
        let slot = Object.keys(this.char.equipment)[slotIdx];

        let cmd = new UnequipItemCommand(this.gm, this.char, slot);
        this.pb.suggestCommand(cmd);
        this.driver.pop();
    }

    protected equipSelectedItem() {
        let item = this.selectedItem as EquippableItem;
        if(!item || !item.equippable) return false;

        let cmd = new EquipItemCommand(this.gm, this.char, item);
        this.pb.suggestCommand(cmd);
        this.driver.pop();
    }
}
