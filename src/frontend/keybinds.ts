import {XY} from "../backend/common";

export enum Action {
    // Movement
    North,
    Northeast,
    East,
    Southeast,
    South,
    Southwest,
    West,
    Northwest,
    // Hotkeys
    Hotkey0,
    Hotkey1,
    Hotkey2,
    Hotkey3,
    Hotkey4,
    Hotkey5,
    Hotkey6,
    Hotkey7,
    Hotkey8,
    Hotkey9,
    // Non-movement actions
    SkipTurn,
    Pickup,
    SaveGame,
    LoadGame,
    // Dialogs
    Confirm,
    Cancel,
    // Other screens and their bindings
    InventoryScreen,
    Drop,
    Use,
    Equip,

    CharacterScreen,
    Unequip,

    AbilityScreen,
    AssignDefaultMelee,
    AssignDefaultRanged,

    DefaultRangedScreen,
    FarLooksScreen,

    // Other
    StopMove,
}

export function actionToDirection(action: Action): XY {
    let x = 0;
    let y = 0;
    switch(action) {
        case Action.North:
            y -= 1;
            break;
        case Action.Northeast:
            y -= 1;
            x += 1;
            break;
        case Action.East:
            x += 1;
            break;
        case Action.Southeast:
            x += 1;
            y += 1;
            break;
        case Action.South:
            y += 1;
            break;
        case Action.Southwest:
            y += 1;
            x -= 1;
            break;
        case Action.West:
            x -= 1;
            break;
        case Action.Northwest:
            x -= 1;
            y -= 1;
            break;
        default:
            return null;
    }
    return {x, y};
}

export class KeyBindings {

    protected keysToActions: { [key: string]: Action[] };
    protected actionToKeysHash: { [action: number]: Array<string>};

    constructor() {
        this.keysToActions = {};
        this.actionToKeysHash = {};
    }

    public bindKeyToAction(key: string, action: Action) {
        key = key.toLowerCase();
        if(!this.keysToActions[key]) {
            this.keysToActions[key] = [];
        }
        this.keysToActions[key].push(action);

        if(!this.actionToKeysHash[action]) {
            this.actionToKeysHash[action] = [];
        }
        this.actionToKeysHash[action].push(key);
    }

    public keyToActions(key: string): Action[] {
        let result = this.keysToActions[key.toLowerCase()];
        if (result === undefined) return [];
        return result;
    }

    public actionToKeys(action: Action): Array<string> {
        let result = this.actionToKeysHash[action];
        if(result === undefined) result = [];
        return result;
    }

    public actionToFirstKey(action: Action): string {
        const result = this.actionToKeys(action);
        if(result === undefined || result.length == 0) return null;
        return result[0];
    }

    public static createDefaultBindings(): KeyBindings {
        let result = new KeyBindings();
        // Reference for key names:
        // https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key/Key_Values

        // North
        result.bindKeyToAction('w', Action.North);
        result.bindKeyToAction('ArrowUp', Action.North);
        result.bindKeyToAction('Up', Action.North);
        result.bindKeyToAction('8', Action.North);
        result.bindKeyToAction('k', Action.North);
        // NE
        result.bindKeyToAction('e', Action.Northeast);
        result.bindKeyToAction('PageUp', Action.Northeast);
        result.bindKeyToAction('9', Action.Northeast);
        result.bindKeyToAction('u', Action.Northeast);
        // East
        result.bindKeyToAction('d', Action.East);
        result.bindKeyToAction('ArrowRight', Action.East);
        result.bindKeyToAction('6', Action.East);
        result.bindKeyToAction('l', Action.East);

        // SE
        result.bindKeyToAction('x', Action.Southeast);
        result.bindKeyToAction('PageDown', Action.Southeast);
        result.bindKeyToAction('3', Action.Southeast);
        result.bindKeyToAction('n', Action.Southeast);
        // South
        result.bindKeyToAction('s', Action.South);
        result.bindKeyToAction('ArrowDown', Action.South);
        result.bindKeyToAction('2', Action.South);
        result.bindKeyToAction('j', Action.South);

        // SW
        result.bindKeyToAction('z', Action.Southwest);
        result.bindKeyToAction('End', Action.Southwest);
        result.bindKeyToAction('1', Action.Southwest);
        result.bindKeyToAction('b', Action.Southwest);
        // West
        result.bindKeyToAction('a', Action.West);
        result.bindKeyToAction('ArrowLeft', Action.West);
        result.bindKeyToAction('Left', Action.West);
        result.bindKeyToAction('4', Action.West);
        result.bindKeyToAction('h', Action.West);
        // NW
        result.bindKeyToAction('q', Action.Northwest);
        result.bindKeyToAction('Home', Action.Northwest);
        result.bindKeyToAction('7', Action.Northwest);
        result.bindKeyToAction('y', Action.Northwest);

        // Hotkeys (off-by-one so visually they start with '1')
        result.bindKeyToAction('!', Action.Hotkey0);
        result.bindKeyToAction('@', Action.Hotkey1);
        result.bindKeyToAction('#', Action.Hotkey2);
        result.bindKeyToAction('$', Action.Hotkey3);
        result.bindKeyToAction('%', Action.Hotkey4);
        result.bindKeyToAction('^', Action.Hotkey5);
        result.bindKeyToAction('&', Action.Hotkey6);
        result.bindKeyToAction('*', Action.Hotkey7);
        result.bindKeyToAction('(', Action.Hotkey8);
        result.bindKeyToAction(')', Action.Hotkey9);

        // Non-movement actions
        result.bindKeyToAction('.', Action.SkipTurn);
        result.bindKeyToAction('Space', Action.SkipTurn);
        result.bindKeyToAction(' ', Action.SkipTurn);

        result.bindKeyToAction(',', Action.Pickup);

        result.bindKeyToAction('[', Action.SaveGame);
        result.bindKeyToAction(']', Action.LoadGame);

        // Dialogs
        result.bindKeyToAction('Enter', Action.Confirm);
        result.bindKeyToAction('Accept', Action.Confirm);

        result.bindKeyToAction('Escape', Action.Cancel);
        result.bindKeyToAction('Esc', Action.Cancel); // older browsers do this
        result.bindKeyToAction('Cancel', Action.Cancel);
        // Inventory
        result.bindKeyToAction('i', Action.InventoryScreen);
        result.bindKeyToAction('r', Action.Drop);
        result.bindKeyToAction('u', Action.Use);
        result.bindKeyToAction('e', Action.Equip);

        // Character screen
        result.bindKeyToAction('c', Action.CharacterScreen);
        result.bindKeyToAction('u', Action.Unequip);

        // Ability screen
        result.bindKeyToAction('t', Action.AbilityScreen);
        result.bindKeyToAction('m', Action.AssignDefaultMelee);
        result.bindKeyToAction('f', Action.AssignDefaultRanged);

        // 'f' ranged screen
        result.bindKeyToAction('f', Action.DefaultRangedScreen);
        result.bindKeyToAction(';', Action.FarLooksScreen);

        // Other
        result.bindKeyToAction('`', Action.StopMove);

        result.warnOnUnbounds();
        return result;
    }

    protected warnOnUnbounds() {
        for (let item in Action) {
            let index = Number(item);
            if (!isNaN(index)) {
                let key = this.actionToFirstKey(index);
                if(key === null) {
                    console.warn(`Unbound action ${Action[index]}`);
                }
            }
        }
    }
}
