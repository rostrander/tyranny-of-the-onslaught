import * as ROT from 'rot-js'
import {GameMaster} from "../backend/gamemaster";
import {XY} from "../backend/common";
import {Tile} from "../backend/world";

export interface MapDrawer {
    gm: GameMaster;

    drawEntireMap(display: ROT.Display, center: XY): void;
    tileAt(loc: XY): Tile;
    drawTileAt(display: ROT.Display, tileLoc: XY, center: XY, forceBg?: string): boolean;
    screenCoordsToMap(screenLoc: XY, center: XY): XY;
}