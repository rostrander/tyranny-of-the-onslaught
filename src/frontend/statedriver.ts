import * as ROT from 'rot-js';
import {DisplayOptions} from "rot-js/lib/display/types";
import {uuidv4, XY} from "../backend/common";
import {Action, KeyBindings} from "./keybinds";


export abstract class Component {
    public loc: XY;

    constructor(public parent: State, where?: XY) {
        this.loc = where || {x: 0, y: 0};
    }

    public abstract handleInput(action: Action, ev: KeyboardEvent): void;

    public abstract render(display: ROT.Display): void;

    public move(dest: XY): void {
        this.loc = {x: dest.x, y: dest.y};
    }
}

export abstract class State {
    protected components: Array<Component>;

    public name: string;

    constructor(public driver: StateDriver) {
        this.components = [];
        this.name = `State ${uuidv4()}`;
    }

    public addComponent(component: Component) {
        this.components.push(component);
    }

    public addRibbon(component: Component) {
        component.loc = {x: 1, y: this.driver.height - 1}
        this.addComponent(component);
    }

    public handleInput(action: Action, ev: KeyboardEvent): void {
        for(let component of this.components) {
            component.handleInput(action, ev);
        }
    }

    public render(display: ROT.Display): void {
        for(let component of this.components) {
            component.render(display);
        }
    }

    protected drawCenter(display: ROT.Display, msg: string, yloc: number, noformat?: string) {
        noformat = noformat ?? msg;
        let halfWidth = Math.floor(this.driver.width / 2);
        display.drawText(
            halfWidth - Math.floor(noformat.length / 2),
            yloc,
            msg
        );
    }

    public frame() {
    }

    public mouseDown(loc: XY) {
    }

    public mouseMove(loc: XY) {
    }

    public mouseUp(loc: XY) {
    }

    public activate(prev: State) {
    };

    public deactivate(nxt: State) {
    };
}

export interface StateSnapshot {
    state: State;
    resolveFunc: (value?: State | PromiseLike<State>) => void;
}

export class StateDriver {
    public display: ROT.Display;
    public state: State;

    public width: number;
    public height: number;

    protected states: Array<StateSnapshot>;
    public keybinds: KeyBindings;

    private raf: () => void;

    constructor(displayOptions?: DisplayOptions) {
        this.display = new ROT.Display(displayOptions);
        this.state = null;
        this.states = [];

        this.width = this.display.getOptions().width;
        this.height = this.display.getOptions().height;

        let container = document.getElementById("content");
        container.appendChild(this.display.getContainer());

        this.keybinds = KeyBindings.createDefaultBindings();

        // Bind keyboard input events
        this.bindEvent('keydown');
        this.bindEvent('keyup');
        this.bindEvent('keypress');

        // Mouse events
        this.bindMouseEvent('mousemove');
        this.bindMouseEvent('mouseup');
    }

    public isAnActiveState(state: State): boolean {
        for(let snapshot of this.states) {
            if(state === snapshot.state) return true;
        }
        return false;
    }

    public pop() {
        let snapshot = this.states.pop();
        let nextState = this.states[this.states.length - 1];
        if (nextState) {
            this.transitionToState(nextState.state, snapshot.state);
        } else {
            this.transitionToState(null, snapshot?.state);
        }
        snapshot.resolveFunc(snapshot.state);
    }

    public push<T extends State>(state: T): Promise<T> {
        state.driver = this;
        let resolver = null;
        let promise = new Promise<T>((resolvefunc) => {
            resolver = resolvefunc;
        });
        let shouldsetupRaf = this.states.length === 0
        this.states.push({state: state, resolveFunc: resolver});
        this.transitionToState(state, this.state);
        if(shouldsetupRaf) this.setupRaf();
        return promise;
    }

    public refresh() {
        this.display.clear();
        if (this.state !== null) {
            this.state.render(this.display);
        }
    }

    protected setupRaf() {
        if(this.raf) return;
        this.raf = () => {
            this.state.frame();
            window.requestAnimationFrame(this.raf);
        };
        window.requestAnimationFrame(this.raf);
    }

    protected bindEvent(event: string) {
        window.addEventListener(event, (e) => {
            // When an event is received, send it to the
            // screen if there is one
            if (this.state !== null && event === 'keyup') {
                // Look up
                let ev = <KeyboardEvent>e;
                let actions = this.keybinds.keyToActions(ev.key);
                for(let action of actions) {
                    this.state.handleInput(action, ev);
                }
                this.refresh();
            }
        });
    }

    protected bindMouseEvent(mouseEvent: string) {
        window.addEventListener(mouseEvent, (e) => {
            let me = e as MouseEvent;
            let [x, y] = this.display.eventToPosition(me);
            let loc = {x, y};
            switch(mouseEvent) {
                case 'mousedown':
                    if (x < 0 || y < 0) return;
                    this.state.mouseDown(loc);
                    break;
                case 'mouseup':
                    if (x < 0 || y < 0) return;
                    this.state.mouseUp(loc);
                    break;
                case 'mousemove':
                    this.state.mouseMove(loc);
                    break;
            }
        });
    }

    protected transitionToState(toState: State, fromState: State) {
        let prev = fromState;
        if (prev) {
            prev.deactivate(toState);
        }
        this.state = toState;
        if (this.state) {
            this.state.activate(fromState);
        }
        this.refresh();
    }


}
