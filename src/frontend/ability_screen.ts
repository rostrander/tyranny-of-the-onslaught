import * as ROT from 'rot-js';

import {State, StateDriver} from "./statedriver";
import {GameMaster} from "../backend/gamemaster";
import {Character} from "../backend/character";
import {ListComponent, RibbonComponent} from "./components";
import {Action} from "./keybinds";

export class AbilityScreen extends State {
    public player: Character;

    protected abilityList: ListComponent;
    protected ribbon: RibbonComponent;

    constructor(driver: StateDriver, protected gm: GameMaster) {
        super(driver);
        this.player = gm.player;

        this.abilityList = new ListComponent(this, this.player.abilities, {x: 1, y: 3});
        this.addComponent(this.abilityList);

        this.ribbon = new RibbonComponent(this, [
            [Action.Cancel, 'quit', this.quit],
            [Action.AssignDefaultMelee, 'assign melee', this.assignMelee],
            [Action.AssignDefaultRanged, 'assign ranged', this.assignRanged],
        ]);
        this.addRibbon(this.ribbon);
    }

    protected quit() {
        this.driver.pop();
    }

    protected assignMelee() {
        let selected = this.abilityList.selectedItem;
        this.player.defaultMeleeAbility = selected.data;
    }

    protected assignRanged() {
        let selected = this.abilityList.selectedItem;
        this.player.defaultRangedAbility = selected.data;
    }

    public render(display: ROT.Display) {
        display.drawText(0, 0, "Your abilities:");

        let selectedAbility = this.abilityList.selectedItem;

        super.render(display);

        let defaultMelee = this.player.defaultMeleeAbility;
        let defaultRanged = this.player.defaultRangedAbility;

        let half = this.driver.width / 2;
        let afterHotkeys = this.driver.height / 2;
        display.drawText(half, 1, `Default Melee:  ${defaultMelee.name}`);
        display.drawText(half, 2, `Default Ranged: ${defaultRanged.name}`);

        if(!selectedAbility) return;
        display.drawText(half, afterHotkeys, selectedAbility.data.description);
    }

}