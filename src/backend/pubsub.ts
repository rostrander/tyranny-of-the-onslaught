import {AddableResult} from "./addable_result";

type PubSubCallback = (msg: PubSubMessage) => void;

export class PubSubMessage {
    public text: string;
    public ar: AddableResult;
    public data: any;

    constructor(public pass = true, details?: AddableResult | string) {
        if(typeof details === "string") {
            this.text = details;
        } else {
            this.ar = details;
        }
    }
}

export class PubSub {
    protected subscribers: PubSubCallback[];

    constructor() {
        this.subscribers = [];
    }

    public sub(callback: PubSubCallback) {
        this.subscribers.push(callback);
    }

    public pub(msg?: PubSubMessage) {
        if(msg === undefined) msg = new PubSubMessage();
        for(let sub of this.subscribers) {
            sub(msg);
        }
    }

    public data(dat: any) {
        let msg = new PubSubMessage();
        msg.data = dat;
        this.pub(msg);
    }

    public msg(text: string) {
        this.pub(new PubSubMessage(true, text));
    }

    public chain(ar: AddableResult) {
        this.pub(new PubSubMessage(true, ar));
    }
}