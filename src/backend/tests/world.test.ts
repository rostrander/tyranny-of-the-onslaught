import {Tile, TileMap} from "../world";
import {Character} from "../character";
import {jsonRoundTrip, TestGameFactory} from "./testutils";
import {GameMaster} from "../gamemaster";
import * as ROT from "rot-js";


describe("TileMap Basics", () => {
    let map: TileMap;

// Actual tests begin
    beforeEach(() => {
        map = new TileMap();
    });

    test("I'm not mixing up row and column major yet again", () => {
        let map = new TileMap(40, 23);
        let mapGen = new ROT.Map.Arena(40, 23);
        mapGen.create(map.rotjsMapCallback());

        expect(map.width).toStrictEqual(40);
        expect(map.height).toStrictEqual(23);
        let tile = map.tileAt({x: 31, y: 20});
        expect(tile).toBeTruthy();
        expect(tile.isNullTile()).toBeFalsy();
    });

    describe("locationInBounds", () => {
        test("In bounds", () => {
            expect(map.locationInBounds({x: 1, y: 1})).toBeTruthy();
        });

        test("Out of bounds negative", () => {
            expect(map.locationInBounds({x: -1, y: 0})).toBeFalsy();
        });

        test("Out of bounds too big", () => {
            expect(map.locationInBounds({x: 0, y: 5000})).toBeFalsy();
        });

        test("Out of bounds on edge", () => {
            const loc = {x: map.width, y: map.height};
            expect(map.locationInBounds(loc)).toBeFalsy();
        });
    });

    describe("rotjsMapCallback", () => {
        test("Happy path", () => {
            let callback = map.rotjsMapCallback();
            // Every generator has 0 = open and 1 = wall
            // (Though Cellular would work better as the opposite)
            callback(0, 0, 0);
            let newTile = map.tileAt({x: 0, y: 0});
            expect(newTile).not.toStrictEqual(Tile.NULL);
            expect(newTile.passable).toBeTruthy();

            callback(1, 0, 1);
            let newerTile = map.tileAt({x: 1, y: 0});
            expect(newerTile).not.toStrictEqual(Tile.NULL);
            expect(newerTile).not.toStrictEqual(newTile);
            expect(newerTile.passable).toBeFalsy();
        });
    });

    describe("setTile", () => {
        let newTile: Tile;

        beforeEach(() => {
            newTile = new Tile('.', {x: 0, y: 1}, true);
        });

        test("Happy path", () => {
            map.setTile(newTile, {x: 0, y: 2});
            expect(map.tileAt({x: 0, y: 2})).toStrictEqual(newTile);
            // Also that should update the tile's location
            expect(newTile.y).toEqual(2);
        });

        test("Happy path default location", () => {
            map.setTile(newTile);
            expect(map.tileAt(newTile)).toStrictEqual(newTile);
            expect(newTile.y).toEqual(1);
        });

        test("Tile adds/removes itself to open list appropriately", () => {
            expect(map.open.size).toStrictEqual(0);
            map.setTile(newTile);
            expect(map.open.size).toStrictEqual(1);
            expect(map.open).toContainEqual(JSON.stringify(newTile.asXY()));
            let worseTile = new Tile('.', {x: 0, y: 1}, false);
            map.setTile(worseTile);
            expect(map.open.size).toStrictEqual(0);
        });

    });

    describe("Open list", () => {
        let newTile: Tile;

        beforeEach(() => {
            newTile = new Tile('.', {x: 3, y: 4}, true);
        });

        test("If no tiles are open, random returns null", () => {
            let chosen = map.randomOpenTile();
            expect(chosen).toBeFalsy();
        });

        describe("Map with open spaces", () => {
            let upTile: Tile;

            beforeEach(() => {
                map.setTile(newTile);
                upTile = new Tile(".", {x: 3, y: 3}, true);
                map.setTile(upTile);
            });

            test("Random open chooses from open list", () => {
                let chosen = map.randomOpenTile();
                expect(chosen).toBeTruthy();
                expect(chosen.x).toStrictEqual(3);
                expect(chosen.y).toBeGreaterThanOrEqual(3);
                expect(chosen.y).toBeLessThanOrEqual(4);
            });
        })
    });
});

describe("World in context", () => {
    let tgf: TestGameFactory;
    let gm: GameMaster;
    let player: Character;
    let map: TileMap;

    beforeEach(() => {
        tgf = new TestGameFactory();
        tgf.newGame();

        ({gm, player, map} = tgf.testGameData());
    });

    describe("addCharacter", () => {

        test("Adds at character's position by default", () => {
            let char = new Character();
            let loc = {x: 2, y: 2};
            char.loc = loc;

            let tile = map.tileAt(loc);
            expect(tile).toBeTruthy();
            expect(tile.character).toBeFalsy();

            expect(map.addCharacter(char)).toBeTruthy();
            expect(tile.character).toBe(char);
        });

        test("Out of bounds fails", () => {
            let char = new Character();
            let loc = {x: -2, y: 2};
            char.loc = loc;
            expect(map.addCharacter(char)).toBeFalsy();

            let tile = map.tileAt(loc);
            expect(tile).toBeTruthy();
            expect(tile.isNullTile()).toBeTruthy();
            expect(tile.character).toBeFalsy();
        });

        test("Adding with location moves it there", () => {
            let char = new Character();
            expect(map.locationInBounds(char)).toBeFalsy();
            let loc = {x: 2, y: 2};

            expect(map.addCharacter(char, loc)).toBeTruthy();
            expect(char.x).toStrictEqual(loc.x);
            expect(char.y).toStrictEqual(loc.y);
        });
    });


    describe("Serialization", () => {
        test("Tiles exclusions on save", () => {
            let charTile = map.tileAt({x: 1, y: 1});
            expect(charTile.character).toBeTruthy();
            let exported = jsonRoundTrip(charTile);
            expect(exported['character']).toBeFalsy();
        });

        test("Tile roundtrip", () => {
            // TODO: When I do items, add to this test
            let someTile = map.tileAt({x: 1, y: 2});
            let exported = jsonRoundTrip(someTile);
            let newTile = Tile.fromJSON(exported);
            expect(newTile).toEqual(someTile);
        });

        test("Map roundtrip", () => {
            let exported = jsonRoundTrip(map);

            let newMap = TileMap.fromJSON(exported);
            expect(newMap.height).toStrictEqual(map.height);
            expect(newMap.width).toStrictEqual(map.width);
            for(let x=0; x < map.width; x++) {
                for(let y=0; y < map.height; y++) {
                    const xy = {x, y}
                    let oldTile = map.tileAt(xy);
                    if(!oldTile.character) {
                        expect(oldTile).toEqual(newMap.tileAt(xy));
                    }
                    expect(newMap.tileAt(xy).character).toBeFalsy();
                }
            }
            // Open tiles set by 'setTile'
            expect(newMap.open).toEqual(map.open);
        });
    });
});
