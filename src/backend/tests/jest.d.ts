import {AddableResult} from "../addable_result";
import {CommandResult} from "../commands";

declare global {
    namespace jest {
        interface Matchers<R> {
            toHaveValue(value: number): R
            toHaveSucceeded(): R
        }
    }
}

export {};
