import {AbstractGameFactory} from "../newgame";
import {GameMaster} from "../gamemaster";
import {Character} from "../character";
import {TileMap} from "../world";
import {PlayerBehavior} from "../behavior";
import {XY} from "../common";
import {EquipItemCommand, MovementCommand} from "../commands";
import {Tradeoff} from "../tradeoff";
import {EquippableItem} from "../item";

interface PlayerMapGm {
    player: Character,
    map: TileMap,
    gm: GameMaster,
    pb: PlayerBehavior,
}

export class TestGameFactory extends AbstractGameFactory {
    public pb: PlayerBehavior;

    newGame(): GameMaster {
        this.easySetup();
        this.player.name = "Testy McTestface";
        this.pb = <PlayerBehavior>this.player.behavior;
        return this.gm;
    }

    public testGameData(): PlayerMapGm {
        if(this.gm === undefined) {
            this.gm = this.newGame();
        }
        return {
            player: this.gm.player,
            map: this.gm.map,
            gm: this.gm,
            pb: this.player.behavior as PlayerBehavior,
        };
    }

    public moveCharacter(who: Character, where: XY) {
        let cmd = new MovementCommand(this.gm, who, where);
        cmd.doMove();
    }

    public statMod(what: string, amount: number, who?: Character) {
        who = who ?? this.player;
        let result = new Tradeoff(
            Tradeoff.mod(what, amount),
            Tradeoff.none(),
        );
        who.addTradeoff(result);
        return result;
    }
}

export function jsonRoundTrip(obj: any): any {
    let strung = JSON.stringify(obj);
    return JSON.parse(strung);
}

export function simpleRoundTripTest(original: any): any {
    let expectedName = original.constructor.name;
    let saved = jsonRoundTrip(original);
    expect(saved['clsName']).toStrictEqual(expectedName);
    let newItem = original.constructor.fromJSON(saved);
    expect(newItem).toEqual(original);
    expect(newItem.constructor.name).toStrictEqual(original.constructor.name);
    return newItem;
}