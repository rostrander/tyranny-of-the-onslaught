import {Character} from "../character";
import {DamageType, Energy} from "../common";
import {jsonRoundTrip, TestGameFactory} from "./testutils";
import {GameMaster} from "../gamemaster";
import {TileMap} from "../world";
import {SitThereBehavior} from "../behavior";
import {EquipItemCommand, SitThereCommand} from "../commands";
import {StatModifier, Tradeoff} from "../tradeoff";
import {Ability, IceSpikeAbility, LightningStrikeAbility} from "../ability";
import {Axe} from "../item";


describe("Character", () => {
    let char: Character;

    beforeEach(() => {
        char = new Character();
    });

    describe("Stats", () => {
        test("Defaults", () => {
            expect(char.str.value).toStrictEqual(10);
            expect(char.dex.value).toStrictEqual(10);
            expect(char.int.value).toStrictEqual(10);

            expect(char.maxHp.value).toStrictEqual(10);
            expect(char.armor.value).toStrictEqual(0);

            expect(char.accuracy.value).toStrictEqual(100);
            expect(char.dodge.value).toStrictEqual(25);

            expect(char.sightDistance.value).toStrictEqual(10);
            expect(char.movementSpeed.value).toStrictEqual(0);
        });

        describe("Derived", () => {
            test("Dex", () => {
                // Dex
                expect(char.dex.value).toStrictEqual(10);
                // Accuracy = dex * 10
                expect(char.accuracy.value).toStrictEqual(100);
                // Dodge = -75 + (dex*10)
                expect(char.dodge.value).toStrictEqual(25);

                char.addTradeoff(new Tradeoff(
                    Tradeoff.mod('dex', 10),
                    Tradeoff.none(),
                ));
                expect(char.dex.value).toStrictEqual(20);
                // Accuracy = dex * 10
                expect(char.accuracy.value).toStrictEqual(200);
                // Dodge = -75 + (dex*10)
                expect(char.dodge.value).toStrictEqual(125);
            });

            test("Str", () => {
                expect(char.str.value).toStrictEqual(10);
                expect(char.armor.value).toStrictEqual(0);

                char.addTradeoff(new Tradeoff(
                    Tradeoff.mod('str', 10),
                    Tradeoff.none(),
                ));
                expect(char.str.value).toStrictEqual(20);
                // Armor = Str * 0.5 - 5
                expect(char.armor.value).toStrictEqual(5);
            });
        });

        test("Tradeoffs", () => {
            let good = Tradeoff.mod('str', 10);
            let bad = Tradeoff.mod('int', -5);
            let tf = new Tradeoff(good, bad);

            char.addTradeoff(tf);
            expect(char.str.value).toStrictEqual(20);
            expect(char.int.value).toStrictEqual(5);

            // Some stats can't go below zero
            bad = Tradeoff.mod('int', -10);
            char.addTradeoff(new Tradeoff(good, bad));
            expect(char.str.value).toStrictEqual(30);
            expect(char.int.value).toStrictEqual(0);

            let none = Tradeoff.none();
            // But you do have to cancel out the negatives before going positive
            good = Tradeoff.mod('int', 3);
            char.addTradeoff(new Tradeoff(good, none));
            expect(char.str.value).toStrictEqual(30);
            expect(char.int.value).toStrictEqual(0);

            // Multiplier
            good = Tradeoff.mod('str', 0.1, true);
            char.addTradeoff(new Tradeoff(good, none));
            expect(char.str.value).toStrictEqual(33);
        })
    });


    describe("Energy", () => {
        beforeEach(() => {
            char.energy = 0;
        });

        test("Starts with zero energy", () => {
            expect(char.energy).toStrictEqual(0);
        });

        test("Adds one energy per tick", () => {
            let origEnergy = char.energy;
            char.tick();
            expect(char.energy).toStrictEqual(origEnergy + 1);
        });

        test("Is ready with zero or more energy", () => {
            expect(char.isReady()).toBeTruthy();
        });

        test("Is not ready with less than zero energy", () => {
            char.energy = -10;
            expect(char.isReady()).toBeFalsy();
        });

        test("Expend expends and renders non-ready", () => {
            let origEnergy = char.energy;
            char.expend(Energy.MOVE);
            expect(char.energy).toStrictEqual(origEnergy - Energy.MOVE);
            expect(char.isReady()).toBeFalsy();
        });

        describe("Movement Speed", () => {
            let origEnergy: number;
            beforeEach(() => {
                origEnergy = char.energy;
            });

            test("Faster / Higher", () => {
                // Higher movement speeds are better (even though it decreases energy spent)
                char.movementSpeed.add(1, 'test');
                char.expend(Energy.MOVE);
                expect(char.energy).toStrictEqual(origEnergy - Energy.MOVE + 1);
                expect(char.isReady()).toBeFalsy();
            });

            test("Effectively capped", () => {
                char.movementSpeed.add(8675309, 'test');
                char.expend(Energy.MOVE);
                expect(char.energy).toStrictEqual(origEnergy - Energy.MIN);
                expect(char.isReady()).toBeFalsy();
            });

            test("Slower / Lower", () => {
                char.movementSpeed.add(-1, 'test');
                char.expend(Energy.MOVE);
                expect(char.energy).toStrictEqual(origEnergy - Energy.MOVE - 1);
                expect(char.isReady()).toBeFalsy();
            });
        });
    });

    describe("Heal", () => {
        test("Regular healing", () => {
            char.hp = 1;
            let healed = char.heal(1);
            expect(healed).toBe(1);
            expect(char.hp).toBe(2);
        });

        test("Overhealing caps at maxhp", () => {
            char.hp = char.maxHp.value - 1;
            let healed = char.heal(10);
            expect(healed).toBe(1);
            expect(char.hp).toBe(char.maxHp.value);
        });
    });

});

describe("Character in context", () => {
    let tgf: TestGameFactory;
    let gm: GameMaster;
    let player: Character;
    let map: TileMap;

    beforeEach(() => {
        tgf = new TestGameFactory();
        tgf.newGame();

        ({gm, player, map} = tgf.testGameData());
    });

    describe("Demonic blood pubsub", () => {
        test("Fires when you take damage", () => {
            let listener = jest.fn();
            player.damageTaken.sub(listener);
            player.takeDamage(4, DamageType.PHYS, player);

            expect(listener).toHaveBeenCalled();
        });
    });

    describe("Behavior", () => {
        test("Default is to return null", () => {
            expect(player.nextCommand()).toBeFalsy();
        });

        describe("SitThereBehavior", () => {
            test("Sits there", () => {
                let stb = new SitThereBehavior(gm);
                player.behavior = stb;
                expect(player.nextCommand()).toBeInstanceOf(SitThereCommand);
            });
        });
    });

    describe("Abilities", () => {
        test("Defaults", () => {
            expect(player.hasAbilityNamed("Basic Melee Attack")).toBeTruthy();
            expect(player.hasAbilityNamed("Basic Ranged Attack")).toBeTruthy();
            expect(player.hasAbilityNamed("Ability Missing")).toBeFalsy();

            expect(player.defaultMeleeAbility).toBeTruthy();
            expect(player.defaultMeleeName).toStrictEqual(player.defaultMeleeAbility.name);

            expect(player.defaultRangedAbility).toBeTruthy();
            expect(player.defaultRangedName).toStrictEqual(player.defaultRangedAbility.name);
        });
    });

    describe("Serialization", () => {
        test("Roundtrip", () => {
            player.behavior = new SitThereBehavior(gm);
            let exported = jsonRoundTrip(player);
            let char = Character.fromJSON(exported);
            expect(char).toMatchObject(exported);
            // Behavior is not restored; the GM will do so.
            expect(char.behavior).toBeFalsy();
        });

        test("Stats recalculated on reloading", () => {
            expect(player.int.value).toStrictEqual(10);
            tgf.statMod('int', 5);
            expect(player.int.value).toStrictEqual(15);

            let axe = new Axe();
            axe.addTradeoff(new Tradeoff(
                new StatModifier('maxWeaponDamage', 1),
                new StatModifier('accuracy', -3)
            ));
            tgf.equip(axe);

            // TODO: have some conditions, whatever else affects stats.

            let exported = jsonRoundTrip(player);
            let char = Character.fromJSON(exported);
            expect(char.int.value).toStrictEqual(15);

            expect(char.minWeaponDamage.value).toEqual(1);
            expect(char.maxWeaponDamage.value).toEqual(7);
            expect(char.accuracy.value).toEqual(97);
            // TODO: Check the stuff I mentioned above
        });

        test("behavior itself excluded", () => {
            let exported = jsonRoundTrip(player);
            expect(exported['behavior']).toBeUndefined();
            expect(exported['_behavior']).toBeUndefined();
        });

        test("behaviorName exported correctly", () => {
            player.behavior = new SitThereBehavior(gm);
            expect(player.behaviorName).toBeTruthy();
            let exported = jsonRoundTrip(player);
            let name = exported['behaviorName']
            expect(name).toBeTruthy();
            expect(name).toStrictEqual("SitThereBehavior");
        });

        test("Abilities realized correctly", () => {
            let strike = new LightningStrikeAbility(gm, player);
            player.gainAbility(strike);
            let exported = jsonRoundTrip(player);
            let char = Character.fromJSON(exported);
            expect(char).toMatchObject(exported);

            expect(char.abilities).toHaveLength(3);  // 2 default, last lightning
            for(let ability of char.abilities) {
                expect(ability).not.toBeInstanceOf(Ability);  // not realized yet
                expect(ability.gm).toBeFalsy();
            }

            gm.addCharacter(char, {x: 3, y: 1});
            // should now be realized
            for(let ability of char.abilities) {
                expect(ability).toBeInstanceOf(Ability);
                expect(ability.gm).toBe(gm);
                expect(ability.src).toBe(char);
            }
        });

        test("Default abilities realized correctly", () => {
            let strike = new LightningStrikeAbility(gm, player);
            player.gainAbility(strike);
            player.defaultMeleeAbility = strike;
            let spike = new IceSpikeAbility(gm, player);
            player.gainAbility(spike);
            player.defaultRangedAbility = spike;
            let exported = jsonRoundTrip(player);

            let char = Character.fromJSON(exported);
            expect(char.defaultMeleeName).toStrictEqual(strike.name);
            expect(char.defaultMeleeAbility).not.toBeInstanceOf(Ability);
            expect(char.defaultRangedName).toStrictEqual(spike.name);
            expect(char.defaultRangedAbility).not.toBeInstanceOf(Ability);

            gm.addCharacter(char, {x: 3, y: 1});
            // should now be realized
            expect(char.defaultMeleeAbility).toBeInstanceOf(Ability);
            expect(char.defaultMeleeAbility.gm).toBe(gm);
            expect(char.defaultMeleeAbility.src).toBe(char);
            expect(char.defaultRangedAbility).toBeInstanceOf(Ability);
            expect(char.defaultRangedAbility.gm).toBe(gm);
            expect(char.defaultRangedAbility.src).toBe(char);
            for(let ability of char.abilities) {
                if(ability.name === strike.name) {
                    expect(ability).toBe(char.defaultMeleeAbility);
                }
                if(ability.name === spike.name) {
                    expect(ability).toBe(char.defaultRangedAbility);
                }
            }
        });

        test("Equipment slots realized correctly", () => {
            let axe = new Axe();
            tgf.equip(axe);
            let exported = jsonRoundTrip(player);
            expect(exported["equipment"]).not.toBeNull();

            let char = Character.fromJSON(exported);
            expect(char.equipment.mainHand).toMatchObject(exported.equipment.mainHand);
            expect(char.equipment.mainHand).toBeInstanceOf(Axe);
        });

        test("Inventory realized correctly", () => {
            let axe = new Axe();
            player.receiveItem(axe);
            let exported = jsonRoundTrip(player);
            expect(exported["inventory"]).not.toBeNull();

            let char = Character.fromJSON(exported);
            expect(char.inventory).toHaveLength(1);
            let item = char.inventory[0];
            expect(item).toMatchObject(exported.inventory[0]);
            expect(item).toBeInstanceOf(Axe);
        });

        test.skip("Tradeoffs realized correctly", () => {

        });
    });
});