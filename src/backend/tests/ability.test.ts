import {jsonRoundTrip, TestGameFactory} from "./testutils";
import {GameMaster} from "../gamemaster";
import {Character} from "../character";
import {PlayerBehavior, SitThereBehavior} from "../behavior";
import {TileMap} from "../world";
import {Ability, BasicMeleeAbility, BasicRangedAbility, IceSpikeAbility, LightningStrikeAbility} from "../ability";
import {RandomService} from "../random_service";
import {Tradeoff} from "../tradeoff";
import {Axe} from "../item";

let tgf: TestGameFactory;
let gm: GameMaster;
let player: Character;
let npc: Character;
let pb: PlayerBehavior;
let map: TileMap;
let rng: RandomService;

let ability: Ability;

beforeEach(() => {
    tgf = new TestGameFactory();
    tgf.newGame();

    ({gm, player, map, pb} = tgf.testGameData());
    player.demonicBlood = 10000;
    npc = tgf.testMonster();
    rng = RandomService.instance;
});

describe("BasicMeleeAbility", () => {
    beforeEach(() => {
        ability = new BasicMeleeAbility(gm, player);
    });

    test("Stats", () => {
        expect(ability.minDamage).toStrictEqual(5);
        expect(ability.maxDamage).toStrictEqual(10);
    })

    test("Happy path", () => {
        rng.fix(3, 2);
        expect(npc.hp).toStrictEqual(10);

        expect(ability.invoke(npc)).toBeTruthy();
        expect(npc.hp).toStrictEqual(8);
    });

    test("Uses melee weapon", () => {
        let basic = player.defaultMeleeAbility;
        expect(basic).toBeTruthy();

        expect(basic.minDamageBonus).toHaveValue(0);
        expect(basic.maxDamageBonus).toHaveValue(0);
        let axe = new Axe();
        tgf.equip(axe);

        basic.recalculateStats();
        expect(basic.minDamageBonus).toHaveValue(1);
        expect(basic.maxDamageBonus).toHaveValue(6);
    });

    test.skip("Does not use ranged weapon", () => {

    });

    test("canCenterAt", () => {
        // Can target things directly next to you
        expect(ability.canCenterAt(npc)).toBeTruthy();
        // Any farther than that, though, and you're out of luck
        expect(ability.canCenterAt({x: 3, y: 2})).toBeFalsy();
    });

    test("Armor Provides Resistance", () => {
        let armor = Tradeoff.mod('armor', 10);
        npc.addTradeoff(new Tradeoff(armor, Tradeoff.none()));
        expect(npc.armor.value).toStrictEqual(10);
        rng.fix(3, 2);
        expect(npc.hp).toStrictEqual(10);

        expect(ability.invoke(npc)).toBeTruthy();
        // 10 Armor + 10 HP = 20 EHP = 50% off
        expect(npc.hp).toStrictEqual(9);
    });

    describe("Serialization", () => {
        test("Roundtrip", () => {
            let exported = jsonRoundTrip(ability);
            let newbility = Ability.fromJSON(exported);
            expect(newbility).toMatchObject(exported);
            expect(newbility).toBeInstanceOf(BasicMeleeAbility);
        });
    });
});

describe("BasicRangedAbility", () => {
    beforeEach(() => {
        ability = new BasicRangedAbility(gm, player);
    });

    test("Stats", () => {
        expect(ability.minDamage).toStrictEqual(2);
        expect(ability.maxDamage).toStrictEqual(5);
        expect(ability.range).toBeGreaterThan(1);
    })

    test("Happy path", () => {
        expect(npc.hp).toStrictEqual(10);
        rng.fix(3, 2);

        expect(ability.invoke(npc)).toBeTruthy();
        expect(npc.hp).toStrictEqual(8);
    });

    test("canCenterAt", () => {
        // Can't target null tiles
        expect(ability.canCenterAt({x: -1, y: -1})).toBeFalsy();
        // Can't target unpassable tiles
        let unpass = {x: 3, y: 4};
        expect(ability.canCenterAt(unpass)).toBeTruthy();  // Can target empty
        gm.map.tileAt(unpass).passable = false;
        expect(ability.canCenterAt(unpass)).toBeFalsy();
        // Can't target yourself, at least with this kind of attack
        expect(ability.canCenterAt(player)).toBeFalsy();
        // Can target enemies, of course.
        expect(ability.canCenterAt(npc)).toBeTruthy();
        // Can't target tiles you can't see
        expect(ability.canCenterAt({x: 25, y: 10})).toBeFalsy();
    });
});

describe("Lightning Strike", () => {
    beforeEach(() => {
        ability = new LightningStrikeAbility(gm, player);
    });

    test("Stats", () => {
        expect(ability.minDamage).toStrictEqual(1);
        expect(ability.maxDamage).toStrictEqual(20);
    });

    test("Happy path", () => {
        rng.fix(3, 2);
        expect(npc.hp).toStrictEqual(10);
        expect(player.demonicBlood).toStrictEqual(10000);

        expect(ability.invoke(npc)).toBeTruthy();
        expect(npc.hp).toStrictEqual(8);
        // Costs 4DB, but player gained 2DB from attack
        expect(player.demonicBlood).toStrictEqual(9998);
    });

    test("Lightning Resistance", () => {
        let lr = Tradeoff.mod('lightningRes', 10);
        npc.addTradeoff(new Tradeoff(lr, Tradeoff.none()));
        expect(npc.lightningRes.value).toStrictEqual(10);
        rng.fix(3, 2);
        expect(npc.hp).toStrictEqual(10);

        expect(ability.invoke(npc)).toBeTruthy();
        // 10 LR + 10 HP = 20 EHP = 50% off
        expect(npc.hp).toStrictEqual(9);
    });

    test("Can't use without enough DB", () => {
        player.demonicBlood = 0;
        expect(ability.invoke(npc)).toBeFalsy();
    });

    test("Demons can use without DB", () => {
        ability = new LightningStrikeAbility(gm, npc);
        expect(npc.demonicBlood).toStrictEqual(0);
        expect(ability.invoke(player)).toBeTruthy();
    });
});

describe("Ice Spike", () => {
    beforeEach(() => {
        ability = new IceSpikeAbility(gm, player);
    });

    test("Stats", () => {
        expect(ability.minDamage).toStrictEqual(5);
        expect(ability.maxDamage).toStrictEqual(5);
        expect(ability.range).toBeGreaterThan(1);
    });

    test("Happy path", () => {
        rng.fix(3, 5);
        expect(npc.hp).toStrictEqual(10);

        expect(ability.invoke(npc)).toBeTruthy();
        expect(npc.hp).toStrictEqual(5);
    });

    test("Spell does not use weapon", () => {
        player.gainAbility(ability);
        expect(ability.minDamageBonus).toHaveValue(0);
        expect(ability.maxDamageBonus).toHaveValue(0);

        let axe = new Axe();
        tgf.equip(axe);
        ability.recalculateStats();

        expect(ability.minDamageBonus).toHaveValue(0);
        expect(ability.maxDamageBonus).toHaveValue(0);
    });

    test("Cold Resistance", () =>{
        let cr = Tradeoff.mod('coldRes', 10);
        npc.addTradeoff(new Tradeoff(cr, Tradeoff.none()));
        expect(npc.coldRes.value).toStrictEqual(10);
        rng.fix(3, 5);
        expect(npc.hp).toStrictEqual(10);

        expect(ability.invoke(npc)).toBeTruthy();
        // Half resistance, damage rounded down
        expect(npc.hp).toStrictEqual(8);
    });
})