import {PubSub, PubSubMessage} from "../pubsub";
import {AddableResult} from "../addable_result";

test("Basic pub/sub", () => {
    let pub = new PubSub();
    let listener = jest.fn();
    pub.sub(listener);
    pub.pub();
    expect(listener).toHaveBeenCalled();
});

test("String messages", () => {
    let pub = new PubSub();
    let msgOuter;
    let listener = (msg: PubSubMessage) => {
        msgOuter = msg.text;
    }
    pub.sub(listener);
    pub.msg("Hello, world!");
    expect(msgOuter).toBeTruthy();
    expect(msgOuter).toStrictEqual("Hello, world!");
});

test("Addable Results", () => {
    let pub = new PubSub();
    let ar = new AddableResult();
    expect(ar.value).toStrictEqual(0);
    let listener = (msg: PubSubMessage) => {
        let arInner = msg.ar;
        arInner.add(5, 'test');
    }
    pub.sub(listener);
    pub.chain(ar);
    expect(ar.value).toStrictEqual(5);
});