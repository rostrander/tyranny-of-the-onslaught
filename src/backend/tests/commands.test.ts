import {TestGameFactory} from "./testutils";
import {GameMaster} from "../gamemaster";
import {Character} from "../character";
import {TileMap} from "../world";
import {
    CompoundCommand,
    EquipItemCommand,
    MovementCommand,
    SitThereCommand,
    UnequipItemCommand,
    UseAbilityCommand
} from "../commands";
import {Energy} from "../common";
import {Ability, BasicMeleeAbility} from "../ability";
import {RandomService} from "../random_service";
import {Axe, EquippableItem, Rapier} from "../item";
import {StatModifier, Tradeoff} from "../tradeoff";

let tgf: TestGameFactory;
let gm: GameMaster;
let player: Character;
let map: TileMap;

beforeEach(() => {
    tgf = new TestGameFactory();
    tgf.newGame();

    gm = tgf.gm;
    player = tgf.player;
    map = tgf.map;
});

describe("SitThereCommand", () => {
    test("execute", () => {
        expect(player.isReady()).toBeTruthy();
        let cmd = new SitThereCommand(gm, player);
        expect(cmd.execute()).toBeTruthy();
        expect(player.isReady()).toBeFalsy();
    })
});

describe("MovementCommand", () => {
    test("Basic movement", () => {
        expect(player.loc).toEqual({x: 1, y: 1});
        let cmd = new MovementCommand(gm, player, {x: 2, y: 1});
        expect(cmd.execute()).toBeTruthy();
        expect(player.isReady()).toBeFalsy();
        // They moved
        expect(player.loc).toEqual({x: 2, y: 1});
        expect(map.tileAt({x:1, y:1}).character).toBeFalsy();
        expect(map.tileAt({x: 2, y: 1}).character).toBe(player);
        // It expended movement energy
        expect(player.energy).toEqual(-Energy.MOVE);
    });

    test("Can't move out of bounds", () => {
        let cmd = new MovementCommand(gm, player, {x: -1, y: 2});
        expect(cmd.execute().success).toBeFalsy();
        expect(player.isReady()).toBeTruthy();
        // They did not move
        expect(player.loc).toEqual({x: 1, y: 1});
        // No expending
        expect(player.energy).toEqual(0);
    });

    test("Can't move into obstacles", () => {
        let tile = map.tileAt({x: 1, y: 2});
        tile.passable = false;

        let cmd = new MovementCommand(gm, player, {x: 1, y: 2});
        expect(cmd.execute().success).toBeFalsy();
        expect(player.isReady()).toBeTruthy();
        // They did not move
        expect(player.loc).toEqual({x: 1, y: 1});
        // No expending
        expect(player.energy).toEqual(0);
    });

    test("Moving into an enemy suggests attacking", () => {
        let npc = tgf.testMonster();
        let cmd = new MovementCommand(gm, player, {x: 2, y: 2});
        let result = cmd.execute();
        expect(result).toBeTruthy();
        expect(result.success).toBeFalsy();
        expect(player.isReady()).toBeTruthy();  // Because we didn't actually do it

        let suggested = result.suggestedCommand;
        expect(suggested).toBeTruthy();
        expect(suggested).toBeInstanceOf(UseAbilityCommand);
    });

    test("Movement recalculates lighting", () => {
        let blocker = map.tileAt({x: 2, y: 2});
        let behind = {x: 3, y: 2};
        blocker.passable = false;
        expect(player.canSee(blocker)).toBeTruthy();
        expect(player.canSee(behind)).toBeTruthy();

        let cmd = new MovementCommand(gm, player, {x: 1, y: 2});
        expect(cmd.execute()).toBeTruthy();
        expect(player.canSee(blocker)).toBeTruthy();  // Can see obstacles themselves
        expect(player.canSee(behind)).toBeFalsy();
        expect(player.canSee(player)).toBeTruthy();
    });
});

describe("UseAbiiltyCommand", () => {
    let rng: RandomService;
    let npc: Character;

    beforeEach(() => {
        rng = RandomService.instance;
        npc = tgf.testMonster();
    });

    test("Happy path", () => {
        let ability: Ability;
        ability = new BasicMeleeAbility(gm, player);
        let cmd = new UseAbilityCommand(gm, player, ability, npc);
        rng.fix(3, 2);
        expect(npc.hp).toStrictEqual(10);

        expect(cmd.execute()).toBeTruthy();
        expect(npc.hp).toStrictEqual(8);
        expect(player.energy).toStrictEqual(-Energy.STANDARD);
    });

    test.skip("Targeting nothing fails", () => {

    });
})

describe("EquipItemCommand", () => {
    test("Happy path", () => {
        let axe = new Axe();
        let cmd = new EquipItemCommand(gm, player, axe);
        // Add a tradeoff for testing purposes
        axe.addTradeoff(new Tradeoff(
            new StatModifier('maxWeaponDamage', 1),
            new StatModifier('accuracy', -3)
        ));

        expect(player.equipment.mainHand).toBeFalsy();
        expect(player.minWeaponDamage.value).toEqual(0);
        expect(player.maxWeaponDamage.value).toEqual(0);
        expect(player.accuracy.value).toEqual(100);

        expect(cmd.execute()).toBeTruthy();

        expect(player.equipment.mainHand).toBe(axe);
        expect(axe.owner).toBe(player);
        expect(player.energy).toStrictEqual(-Energy.STANDARD);

        // Should have applied its intrinsics and tradeoffs to the player:
        expect(player.minWeaponDamage.value).toEqual(1);
        expect(player.maxWeaponDamage.value).toEqual(7);
        expect(player.accuracy.value).toEqual(97);
    });

    test("Equipping an item takes it out of inventory", () => {
        let axe = new Axe();
        player.receiveItem(axe);
        expect(player.inventory).toHaveLength(1);
        expect(player.inventory[0]).toBe(axe);

        let cmd = new EquipItemCommand(gm, player, axe);
        expect(cmd.execute()).toBeTruthy();

        expect(player.equipment.mainHand).toBe(axe);
        expect(player.inventory).toHaveLength(0);
    });

    test("Equipping a new thing un-equips old thing", () => {
        let axe = new Axe();
        let cmd = new EquipItemCommand(gm, player, axe);

        expect(cmd.execute()).toHaveSucceeded();
        let rapier = new Rapier();
        let reequip = new EquipItemCommand(gm, player, rapier);
        let result = reequip.execute();

        expect(result).not.toHaveSucceeded();
        expect(player.equipment.mainHand).toBe(axe);

        let suggest = result.suggestedCommand;
        expect(suggest).toBeInstanceOf(CompoundCommand);
        let cmds = (suggest as CompoundCommand).commands
        expect(cmds).toHaveLength(2);
        // Commands are backwards to make for easy `pop`ing
        expect(cmds[1]).toBeInstanceOf(UnequipItemCommand);
        expect(cmds[0]).toBeInstanceOf(EquipItemCommand);

        let result2 = suggest.execute();
        expect(result2).toHaveSucceeded();

        expect(player.equipment.mainHand).toBe(rapier);
    });
});

describe("UnequipItemCommand", () => {
    test("Happy path", () => {
        let axe = tgf.equip(new Axe());
        let cmd = new UnequipItemCommand(gm, player, "mainHand");

        expect(cmd.execute()).toHaveSucceeded();
        expect(player.equipment.mainHand).toBeNull();
        expect(player.inventory).toHaveLength(1);
        expect(player.inventory[0]).toBe(axe);
    });

    test("Unequipping nothing does nothing, but it succeeds", () => {
        let cmd = new UnequipItemCommand(gm, player, "mainHand");

        expect(cmd.execute()).toHaveSucceeded();
        expect(player.equipment.mainHand).toBeNull();
        expect(player.inventory).toHaveLength(0);
    });
});