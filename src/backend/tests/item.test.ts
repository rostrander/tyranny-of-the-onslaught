import {jsonRoundTrip, simpleRoundTripTest, TestGameFactory} from "./testutils";
import {GameMaster} from "../gamemaster";
import {Character} from "../character";
import {Axe, Item, Weapon} from "../item";
import {StatModifier, Tradeoff} from "../tradeoff";

let tgf: TestGameFactory;
let gm: GameMaster;
let player: Character;

beforeEach(() => {
    tgf = new TestGameFactory();
    ({player} = tgf.testGameData());
})

describe("Serialization", () => {
    test("Basic roundtrip", () => {
        let junk = new Item('junk', '?');
        let saved = jsonRoundTrip(junk);
        let newJunk = Item.fromJSON(saved);
        expect(newJunk).toEqual(junk);
    });

    test("Weapon roundtrip", () => {
        let oldAxe = new Axe();
        oldAxe.addTradeoff(new Tradeoff(
            new StatModifier('maxWeaponDamage', 1),
            new StatModifier('accuracy', -3)
        ));
        let axe = simpleRoundTripTest(oldAxe);

        expect(axe.intrinsic).toHaveLength(2);
        expect(axe.intrinsic[0]).toBeInstanceOf(StatModifier);

        expect(axe.tradeoffs).toHaveLength(1);
        expect(axe.tradeoffs[0]).toBeInstanceOf(Tradeoff);
        expect(axe.tradeoffs[0].pos).toBeInstanceOf(StatModifier);
    });

    test("owner doesn't result in loops", () => {
        let axe = new Axe();
        tgf.equip(axe);
        let exported = jsonRoundTrip(axe);
        expect(exported).toBeTruthy();
        expect(exported.owner).toBeFalsy();
    });
});
