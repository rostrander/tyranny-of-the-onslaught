import {DamageType, exportJsonExcept, Faction, MessageBuffer, MultiMessageBuffer, Point, XY} from "./common";
import {Character} from "./character";
import {GameMaster} from "./gamemaster";
import {RandomService} from "./random_service";
import {AddableResult} from "./addable_result";
import {SerializableCls, SerializableRegistry} from "./registry";

class AbilityResult {
    public hit: Character;

    public srcMsgs: MessageBuffer;
    public targetMsgs: MessageBuffer;
    public both: MultiMessageBuffer;

    constructor(public src: Character, public target: Character) {
        this.srcMsgs = new MessageBuffer();
        this.targetMsgs = new MessageBuffer();
        this.both = new MultiMessageBuffer(this.srcMsgs, this.targetMsgs);
    }

    public sendMessages() {
        this.srcMsgs.sendMessages(this.src);
        this.targetMsgs.sendMessages(this.target);
    }
}

export const ABILITIES = new SerializableRegistry<Ability>();
const registerAbility = ABILITIES.registryDecorator();

type AbilityTemplate = Partial<Ability> & SerializableCls;

export abstract class Ability {
    public internalName: string;
    public range: number;
    public description: string;

    protected rng: RandomService;

    public minDamageBonus: AddableResult;
    public maxDamageBonus: AddableResult;

    public damageType = DamageType.PHYS;
    abstract minDamage: number;
    abstract maxDamage: number;

    public dbCost: number;
    public clsName: string;

    public isAttack: boolean;
    public isSpell: boolean;

    public static fromJSON(template: AbilityTemplate): Ability {
        return ABILITIES.newFromTemplate(template);
    }

    constructor(public name = "ERROR", public gm?: GameMaster, public src?: Character) {
        // for now
        this.internalName = name;
        this.rng = RandomService.instance;
        this.range = 1;
        this.dbCost = 0;
        this.clsName = this.constructor.name;
        this.isAttack = true;
        this.recalculateStats();
    }

    protected beSpell() {
        this.isSpell = true;
        this.isAttack = false;
    }

    public recalculateStats() {
        this.maxDamageBonus = new AddableResult();
        this.minDamageBonus = new AddableResult();
        if(this.src) {
            if(this.isAttack) {
                this.minDamageBonus.add(this.src.minWeaponDamage.value, "weapon");
                this.maxDamageBonus.add(this.src.maxWeaponDamage.value, "weapon");
            }
        }
    }

    public canCenterAt(where: XY): boolean {
        let tile = this.gm.map.tileAt(where);
        if(tile.isNullTile()) return false;
        if(!tile.passable) return false;
        if(!this.src.canSee(tile)) return false;
        let dist = Point.distance(this.src, tile);
        if(dist > this.range) return false;
        let who = tile.character;
        if(who) {
            if(who.faction === this.src.faction) return false;
        }
        return true;
    }

    protected getWho(where: XY): Character {
        let tile = this.gm.map.tileAt(where);
        let who = tile.character;
        return who;
    }

    protected rollToHit(result: AbilityResult): AbilityResult {
        let who = result.target;
        let tohit = this.src.accuracy.value - who.dodge.value;
        let roll = this.rng.roll();
        let rollMsg =`Rolled ${roll} (<${tohit} to hit), `;
        result.both.appendMessage(rollMsg);
        // For now we're going to ignore the outliers (i.e. > 100% or < 0%)
        if (roll <= tohit) {
            result.hit = who;
            result.both.appendMessage("HIT ");
        } else {
            result.both.appendMessage("MISS ");
        }
        return result;
    }

    protected rollDamage(result: AbilityResult) {
        let min = this.minDamage + this.minDamageBonus.value;
        let max = this.maxDamage + this.maxDamageBonus.value;

        let dmg = this.rng.roll(min, max);
        this.doDamage(result, dmg, this.damageType)
    }

    protected doDamage(result: AbilityResult, amount: number, type: DamageType) {
        if(amount < 1) amount = 1;
        let dmgMsg = ` for ${amount} damage`
        result.both.appendMessage(dmgMsg);
        result.target.takeDamage(amount, type, this.src);

        if(!result.target.alive) {
            result.srcMsgs.startMessage(`${result.target.name} dies!`)
            result.targetMsgs.startMessage("You die!");
        }
    }

    public canPayCosts(): boolean {
        if(this.src.faction === Faction.DEMON) return true;
        return this.src.demonicBlood >= this.dbCost;
    }

    public payCosts(): boolean {
        if(!this.canPayCosts()) return false;
        this.src.demonicBlood -= this.dbCost;
        return true;
    }

    public invoke(where: XY): boolean {
        this.recalculateStats();
        let who = this.getWho(where);
        if(!who) return false;
        if(!this.payCosts()) {
            this.src.messages.msg(`Not enough DB to use ${this.name} (need ${this.dbCost})`);
            return false;
        }
        let result = new AbilityResult(this.src, who);
        result.srcMsgs.startMessage(`You use ${this.name} at ${who.name}: `);
        result.targetMsgs.startMessage(`${this.src.name} uses ${this.name} at you: `);

        // Tohit
        this.rollToHit(result);

        if(result.hit) {
            this.rollDamage(result);
        }
        result.sendMessages();
        return true;
    }

    toJSON() {
        let result = exportJsonExcept(this,
            'src', 'gm'
        )
        return result;
    }
}

@registerAbility
export class BasicMeleeAbility extends Ability {

    constructor(gm?: GameMaster, src?: Character) {
        super("Basic Melee Attack", gm, src);
        this.description = "A simple strike with your weapon";
    }

    public get minDamage(): number {
        let str = this.src.str.value;
        return Math.floor(str / 2);
    }

    public get maxDamage(): number {
        let str = this.src.str.value;
        return str;
    }

}

@registerAbility
export class BasicRangedAbility extends Ability {
    constructor(gm?: GameMaster, src?: Character) {
        super("Basic Ranged Attack", gm, src);
        this.description = "A simple shot with your weapon";
        this.range = 10;
    }

    public get minDamage(): number {
        let dex = this.src.dex.value;
        return Math.floor(dex / 4);
    }

    public get maxDamage(): number {
        let dex = this.src.dex.value;
        return Math.floor(dex / 2);
    }
}

// Non-basic abilities follow

@registerAbility
export class LightningStrikeAbility extends Ability {

    damageType = DamageType.LIGHTNING;

    constructor(gm?: GameMaster, src?: Character) {
        super("Lightning Strike", gm, src);
        this.description = "Imbue your weapon strike with the power of lightning"
        this.dbCost = 4;
    }

    public minDamage = 1;

    public get maxDamage(): number {
        let str = this.src.str.value;
        return str + 10;
    }
}

@registerAbility
export class IceSpikeAbility extends Ability {

    damageType = DamageType.COLD;

    constructor(gm?: GameMaster, src?: Character) {
        super("Ice Spike", gm, src);
        this.description = "Shoot a spike of ice at an enemy";
        this.dbCost = 4;
        this.range = 10;
        this.beSpell();
    }

    public get minDamage() {
        let int = this.src.int.value;
        return int/2;
    }

    public get maxDamage() {
        return this.minDamage;
    }
}