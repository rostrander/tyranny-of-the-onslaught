interface AddableResultEntry {
    amount: number;
    reason: string;
    longReason?: string;
    src?: string;
    fail?: boolean;
}

export class AddableResult {

    public originalPass: boolean;
    public additions: AddableResultEntry[];
    public multipliers: AddableResultEntry[];
    public minimum: number;
    public maximum: number;

    constructor(public originalValue = 0, public originalMult = 1) {
        this.originalPass = true;
        this.additions = [];
        this.multipliers = [];
    }

    public get passes(): boolean {
        let result = this.originalPass;
        for(let added of this.additions) {
            result = result && (!added.fail);
        }
        return result;
    }

    public get value(): number {
        let result = this.totalValue();
        if(this.minimum !== undefined && result < this.minimum) return this.minimum;
        if(this.maximum !== undefined && result > this.maximum) return this.maximum;
        return result;
    }

    public add(amount: number, reason: string, longReason?: string, src?: string) {
        const addition = {
            amount,
            reason,
            longReason,
            src,
        };
        this.additions.push(addition);
        return this;
    }

    public addMultiplier(amount: number, reason: string, longReason?: string, src?: string) {
        const addition = {
            amount,
            reason,
            longReason,
            src,
        };
        this.multipliers.push(addition);
        return this;
    }

    public addBoolean(pass: boolean, reason: string, longReason?: string, src?: string) {
        const addition = {
            amount: 0,
            reason,
            longReason,
            src,
            fail: !pass,
        }
        this.additions.push(addition);
        return this;
    }

    public fail(reason: string, longReason?: string, src?: string) {
        this.addBoolean(false, reason, longReason, src);
    }

    protected totalValue() {
        let total = this.originalValue;
        for (let added of this.additions) {
            total += added.amount;
        }
        let mult = 1.0;
        for(let mults of this.multipliers) {
            mult += mults.amount;
        }
        if(mult < 0) mult = 0;
        total *= mult;
        return total;
    }
}