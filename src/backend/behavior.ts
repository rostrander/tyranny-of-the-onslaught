import {GameMaster} from "./gamemaster";
import {Character} from "./character";
import {Command, CommandFactory, MovementCommand, SitThereCommand, UseAbilityCommand} from "./commands";
import {ClassRegistry} from "./registry";
import {Faction, Point, XY} from "./common";
import {Ability} from "./ability";

export const BEHAVIORS = new ClassRegistry<Behavior>();
export const registerBehavior = BEHAVIORS.registryDecorator();

export abstract class Behavior {
    public char: Character;
    private _target: Character;
    private _cmd: CommandFactory;
    protected lastSawTarget: XY;

    public constructor(public gm: GameMaster) {
    }

    public get cmd(): CommandFactory {
        if(!this._cmd) this._cmd = new CommandFactory(this.gm, this.char);
        return this._cmd;
    }

    public get target(): Character {
        if(!this._target || !this._target.alive) {
            this._target = this.findTarget();
        }
        return this._target;
    }

    public findTarget(): Character {
        return this.gm.player;
    }

    public sitThere(): Command {
        return new SitThereCommand(this.gm, this.char);
    }

    public canSeeTarget(): boolean {
        return this.char.canSee(this.target) > 0;
    }

    public distanceToTarget(): number {
        return Point.distance(this.target.loc, this.char.loc);
    }

    public attackTargetWith(ability?: Ability): Command {
        ability = ability || this.char.defaultMeleeAbility;
        let attack = new UseAbilityCommand(this.gm, this.char, ability, this.target);
        return attack;
    }

    public moveTowardTarget(orLoc?: XY): Command {
        let targetLoc = orLoc || this.target;
        let nextStep = this.cmd.stepToward(this.char, targetLoc, false);
        return nextStep;
    }

    abstract act(): Command;
}

@registerBehavior
export class SitThereBehavior extends Behavior {
    act(): Command {
        return this.sitThere();
    }
}

export class PlayerBehavior extends Behavior {
    public cmdQueue: Command[];

    constructor(gm: GameMaster) {
        super(gm);
        this.cmdQueue = [];
    }

    act(): Command {
        if(this.cmdQueue.length === 0) return null;
        return this.cmdQueue.shift();
    }

    public suggestCommand(cmd: Command): boolean {
        if(this.cmdQueue.length > 0) return false;
        return this.addCommand(cmd);
    }

    public addCommand(...cmd: Command[]) {
        this.cmdQueue.push(...cmd);
        return true;
    }
}

export class MeleeCombatBehavior extends Behavior {
    act(): Command {
        let acted: Command;
        if(!this.target) return this.sitThere();
        if(!this.canSeeTarget()) {
            if(this.lastSawTarget) {
                if(Point.sameAs(this.char.loc, this.lastSawTarget)) {
                    // We're there, but they're gone
                    this.lastSawTarget = null;
                    return this.sitThere();
                }
                return this.moveTowardTarget(this.lastSawTarget);
            }
            return this.sitThere();
        } else {
            this.lastSawTarget = this.target;
        }

        if(this.distanceToTarget() <= 1) {
            acted = this.attackTargetWith();
        } else {
            acted = this.moveTowardTarget();
        }
        if(!acted) return this.sitThere();

        return acted;
    }

}