import * as ROT from 'rot-js'

export class RandomService {
    protected static _instance: RandomService;
    public static get instance() {
        if(!this._instance) this._instance = new RandomService();
        return this._instance
    }

    protected fixedRolls: number[];

    constructor() {
        this.fixedRolls = [];
    }

    public fix(...desired: number[]) {
        this.fixedRolls.push(...desired);
    }

    // Inclusive
    roll(min=1, max=100) {
        let result;
        if(this.fixedRolls.length > 0) {
            result = this.fixedRolls.shift();
            //console.log("Consumed fixed roll", result);
            return result;
        } else {
            result = ROT.RNG.getUniformInt(min, max);
            //console.log("Random roll", result);
        }
        return result;
    }
}