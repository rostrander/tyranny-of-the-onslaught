import {AddableResult} from "./addable_result";

export interface StatTarget {
    getStat(named: string): AddableResult;
}

export class StatModifier {
    public static fromJSON(template: Partial<StatModifier>): StatModifier {
        return new StatModifier(template.statName, template.value, template.amountIsMultiplier);
    }

    constructor(public statName: string, public value: number, public amountIsMultiplier = false) {
    }

    public get name() {
        if(this.value < 0) {
            return `${this.value} ${this.statName}`;
        }
        return `+${this.value} ${this.statName}`;
    }

    public applyModifier(target: StatTarget) {
        let result = target.getStat(this.statName);
        if(this.amountIsMultiplier) {
            result.addMultiplier(this.value, this.name);
        } else {
            result.add(this.value, this.name);
        }
    }
}

export class Tradeoff {

    public static fromJSON(template: Partial<Tradeoff>) {
        let pos = StatModifier.fromJSON(template.pos);
        let neg = StatModifier.fromJSON(template.neg);
        return new Tradeoff(pos, neg, template.name);
    }

    public static none(statName = 'str'): StatModifier {
        return this.mod(statName, 0);
    }

    public static mod(statName: string, value: number, amountIsMultiplier = false): StatModifier {
        return new StatModifier(statName, value, amountIsMultiplier);
    }

    constructor(public pos: StatModifier, public neg: StatModifier, public name?: string) {
        if(!this.name) this.name = `${pos.name}/${neg.name}`;
    }

    apply(target: StatTarget) {
        this.pos.applyModifier(target);
        this.neg.applyModifier(target);
    }

}