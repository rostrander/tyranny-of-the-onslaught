export interface RegistryMappable {
    name: string;
}

interface BareConstructor<T> {
    new(...args: any): T;
}

export interface SerializableCls {
    clsName: string;
}

interface SerializableConstructor<T> extends BareConstructor<T> {
    fromJSON(template: Partial<T>): SerializableCls;
}


export class ClassRegistry<T> {
    private storage: {[id: string]: BareConstructor<T>}

    constructor() {
        this.storage = {};
    }

    constructorFor(name: string) {
        return this.storage[name];
    }

    registryDecorator() {
        return (constructor: BareConstructor<T>) => {
            this.storage[constructor.name] = constructor;
        }
    }
}

export class SerializableRegistry<T> {

    private storage: {[id: string]: SerializableConstructor<T>}

    constructor() {
        this.storage = {};
    }

    /* Output */
    newFromTemplate(template: SerializableCls): T {
        let clsName = template.clsName;
        let cls = this.constructorFor(clsName);
        let result = new cls()
        Object.assign(result, template);
        return result;
    }

    constructorFor(name: string): SerializableConstructor<T> {
        let result = this.storage[name];
        if(!result) {
            console.warn("Could not find constructor for", name);
        }
        return result;
    }

    registryDecorator() {
        return (constructor: SerializableConstructor<T>) => {
            this.storage[constructor.name] = constructor;
        }
    }


}