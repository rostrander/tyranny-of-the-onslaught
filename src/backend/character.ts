import {Color} from "rot-js/lib/color";
import {DamageType, Energy, exportJsonExcept, Faction, removeFromArray, XY} from "./common";
import {Command} from "./commands";
import {Behavior, BEHAVIORS} from "./behavior";
import {AddableResult} from "./addable_result";
import {StatTarget, Tradeoff} from "./tradeoff";
import {PubSub} from "./pubsub";
import {GameMaster} from "./gamemaster";
import {Ability, BasicMeleeAbility, BasicRangedAbility} from "./ability";
import {EquippableItem, Item} from "./item";

export interface EquipmentSlots {
    head: EquippableItem,
    neck: EquippableItem,
    body: EquippableItem,
    hands: EquippableItem,
    waist: EquippableItem,
    feet: EquippableItem,
    ring1: EquippableItem,
    ring2: EquippableItem,
    mainHand: EquippableItem,
    offHand: EquippableItem,
    [slotName: string]: EquippableItem,
}

export class Character implements StatTarget {
    public glyph: string;
    public name: string;
    public internalName: string;
    public description: string;
    public fgcolor: Color;

    //region Stats
    public str: AddableResult;
    public dex: AddableResult;
    public int: AddableResult;

    public maxHp: AddableResult;

    public accuracy: AddableResult;
    public dodge: AddableResult;

    public armor: AddableResult;
    public lightningRes: AddableResult;
    public coldRes: AddableResult;

    public sightDistance: AddableResult;
    public movementSpeed: AddableResult;

    // Weapon specific stats
    public minWeaponDamage: AddableResult;
    public maxWeaponDamage: AddableResult;

    //endregion
    public tradeoffs: Tradeoff[];
    public abilities: Ability[];
    public defaultMeleeName: string;
    public defaultRangedName: string;
    protected _defaultMeleeAbility: Ability;
    protected _defaultRangedAbility: Ability;

    public level: number;
    public energy: number;
    public hp: number;
    public tempHp: number;
    public demonicBlood: number;
    public faction: Faction;
    public equipment: EquipmentSlots;
    public inventory: Item[];

    // Going to try making Character an XY this time around
    public x: number;
    public y: number;

    // Other
    public behaviorName: string;
    protected _behavior: Behavior;
    public messages: PubSub;
    public gm: GameMaster;
    public damageTaken: PubSub;

    public visibleTiles: {[key: string]: number};

    public static fromJSON(template: Partial<Character>): Character {
        let result = new Character(template);

        // Establish everything else (e.g. realizing conditions or whatever)
        // Things that need a GM are realized in characterAddedBy
        result.recalculateStats();

        // Equipment
        for(let slot in template.equipment) {
            let item = template.equipment[slot];
            if(item) {
                let realItem = Item.fromJSON(item) as EquippableItem;
                result.equipment[slot] = realItem;
                realItem.onEquippedBy(result);
            }
        }

        // Items
        result.inventory = template.inventory.map(i => Item.fromJSON(i));

        return result;
    }

    constructor(overrides?: Partial<Character>) {
        this.level = 1;
        this.energy = Energy.ROLL_INITIATIVE;
        this.tempHp = 0;
        this.demonicBlood = 0;
        this.faction = Faction.DEMON;

        this.x = -1;
        this.y = -1;

        this.tradeoffs = [];
        this.abilities = [

        ];
        this.visibleTiles = {};

        if(overrides) {
            Object.assign(this, overrides);
        }

        this.equipment = {
            head: null,
            neck: null,
            body: null,
            hands: null,
            waist: null,
            feet: null,
            ring1: null,
            ring2: null,
            mainHand: null,
            offHand: null,
        };
        this.inventory = [];

        this.recalculateStats();
        this.hp = overrides?.hp ?? this.maxHp.value;

        this.messages = new PubSub();
        this.damageTaken = new PubSub();
    }

    public get behavior(): Behavior {
        return this._behavior;
    }

    public set behavior(behavior: Behavior) {
        behavior.char = this;
        this._behavior = behavior;
        this.behaviorName = behavior.constructor.name;
    }

    public get defaultMeleeAbility(): Ability {
        return this._defaultMeleeAbility;
    }

    public set defaultMeleeAbility(ability: Ability) {
        this._defaultMeleeAbility = ability;
        this.defaultMeleeName = ability.name;
    }

    public get defaultRangedAbility(): Ability {
        return this._defaultRangedAbility;
    }

    public set defaultRangedAbility(ability: Ability) {
        this._defaultRangedAbility = ability;
        this.defaultRangedName = ability.name;
    }

    public get alive(): boolean {
        return this.hp > 0;
    }

    public get loc() {
        return {x: this.x, y: this.y};
    }

    public set loc(where: XY) {
        this.x = where.x;
        this.y = where.y;
    }

    public statusKey(): string {
        return `${this.level}:${this.hp}:${this.maxHp}:${this.demonicBlood}`
    }

    public characterAddedBy(gm: GameMaster) {
        this.gm = gm;
        // Behaviors
        let cls = BEHAVIORS.constructorFor(this.behaviorName);
        if(cls) {
            this.behavior = new cls(gm);
        }

        // Abilities
        if(this.abilities.length > 0) {
            let realized = [];
            for(let ability of this.abilities) {
                let real = Ability.fromJSON(ability);
                real.gm = gm;
                real.src = this;
                realized.push(real);

                if(real.name == this.defaultMeleeName) {
                    this.defaultMeleeAbility = real;
                }
                if(real.name == this.defaultRangedName) {
                    this.defaultRangedAbility = real;
                }
            }
            this.abilities = realized;
        } else {
            let bma = new BasicMeleeAbility(gm, this);
            this.gainAbility(bma);
            this.defaultMeleeAbility = bma;
            let bra = new BasicRangedAbility(gm, this);
            this.gainAbility(bra);
            this.defaultRangedAbility = bra;
        }

        // Not needed now, but probably when we realize e.g. armor
        this.recalculateStats();
    }

    public recalculateStats() {
        // Set base / defaults
        this.str = new AddableResult(10);
        this.str.minimum = 0;
        this.dex = new AddableResult(10);
        this.dex.minimum = 0;
        this.int = new AddableResult(10);
        this.int.minimum = 0;

        this.maxHp = new AddableResult(10);
        this.armor = new AddableResult(0);
        this.sightDistance = new AddableResult(10);

        this.accuracy = new AddableResult(100);
        this.dodge = new AddableResult(25);

        this.armor = new AddableResult();
        this.lightningRes = new AddableResult();
        this.coldRes = new AddableResult();

        this.sightDistance = new AddableResult(10);
        this.movementSpeed = new AddableResult(0);

        this.minWeaponDamage = new AddableResult();
        this.maxWeaponDamage = new AddableResult();

        // Realize persisted fields
        this.tradeoffs = this.tradeoffs.map( (tr) => {
            return Tradeoff.fromJSON(tr);
        });

        // Apply all possible changes
        for(let tradeoff of this.tradeoffs) {
            if(tradeoff['apply']) tradeoff.apply(this);
        }

        // Recalculate derived stats
        //   Str derived
        let str = this.str.value;
        this.armor.originalValue = Math.floor(str * 0.5) - 5;
        //   Dex derived
        let dex = this.dex.value;
        this.accuracy.originalValue = dex * 10;
        this.dodge.originalValue = -75 + (dex * 10);

        // Int derived
        let int = this.int.value;
        this.lightningRes.originalValue = 0;  // TODO: Update when I figure out this conversion rate
        this.coldRes.originalValue = 0;
    }

    public addTradeoff(tradeoff: Tradeoff) {
        this.tradeoffs.push(tradeoff);
        this.recalculateStats();
    }

    public getStat(named: string): AddableResult {
        let ar = (this as any)[named] as AddableResult;
        if(!ar) console.error("Could not look up stat", named);
        return ar;
    }

    public canSee(where: XY): number {
        let key = `${where.x},${where.y}`;
        let result = this.visibleTiles[key] || 0;
        return result;
    }

    public setSight(where: XY, amount: number) {
        if(amount === 0) return;
        let key = `${where.x},${where.y}`;
        this.visibleTiles[key] = amount;
    }

    public receiveItem(...items: Item[]): void {
        this.inventory.push(...items);
    }

    public removeItem(...items: Item[]): boolean {
        for(let item of items) {
            let removed = removeFromArray(item, this.inventory);
            if(!removed) return false;
        }
        return true;
    }

    public takeDamage(amount: number, type: DamageType, fromWho: Character): number {
        if(amount < 1) amount = 1;
        let res = this.calculateResist(type);
        amount = Math.floor(amount * (1 - res));
        if(amount > this.hp) amount = this.hp;
        if(amount < 1) amount = 1;
        this.hp -= amount;
        this.damageTaken.chain(new AddableResult(amount));
        return amount;
    }

    public calculateResist(type: DamageType): number {
        let protection = 0;
        switch (type) {
            case DamageType.PHYS:
                protection = this.armor.value;
                break;
            case DamageType.LIGHTNING:
                protection = this.lightningRes.value;
                break;
            case DamageType.COLD:
                protection = this.coldRes.value;
                break;
            default:
                break;
        }
        let ehp = this.maxHp.value + protection;
        return protection / ehp;
    }

    public heal(amount: number): number {
        amount = Math.floor(amount);
        if(amount <= 0) return 0;

        let target = this.hp + amount;
        if(target > this.maxHp.value) {
            amount -= (target - this.maxHp.value);
        }
        this.hp += amount;
        return amount;
    }

    public isReady(): boolean {
        return this.energy >= 0;
    }

    public tick(numTurns = 1): boolean {
        this.energy += numTurns;
        return this.isReady();
    }

    public expend(amount: number, original?: number): number {
        if(original === undefined) original = amount;
        // Apply movement speed
        let result = amount;
        if(original === Energy.MOVE) {
            result = Math.max(Energy.MIN, result - this.movementSpeed.value);
        }

        this.energy -= result;
        return this.energy;
    }

    public nextCommand(): Command {
        if(!this.behavior) return null;
        return this.behavior.act();
    }

    public hasAbilityNamed(name: string) {
        for(let ability of this.abilities) {
            if(ability.name === name) return ability;
        }
        return false;
    }

    public gainAbility(ability: Ability): Ability {
        this.abilities.push(ability);
        return ability;
    }

    public toJSON() {
        let result = exportJsonExcept(this,
            'behavior', '_behavior', 'visibleTiles',
            'messages', 'gm', 'damageTaken',
        )
        return result;
    }
}