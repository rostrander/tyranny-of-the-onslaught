import * as ROT from 'rot-js'

import {GameMaster} from "./gamemaster";
import {Character, EquipmentSlots} from "./character";
import {Energy, Faction, Point, XY} from "./common";
import {Ability, BasicRangedAbility} from "./ability";
import {EquippableItem} from "./item";

export class CommandResult {
    public suggestedCommand: Command;

    public static get pass() {
        return new CommandResult(true);
    }

    public static get fail() {
        return new CommandResult(false);
    }

    constructor(public success = true) {
    }

    suggest(cmd: Command, newSuccess=false) {
        this.suggestedCommand = cmd;
        this.success = newSuccess;
        return this;
    }
}

export abstract class Command {
    constructor(public gm: GameMaster, public src: Character) {
    }

    abstract execute(): CommandResult;
}

export class CommandFactory {
    constructor(public gm: GameMaster, public char: Character) {
    }

    sitThere(): Command {
        return new SitThereCommand(this.gm, this.char);
    }

    superMove(...dirs: XY[]): Command[] {
        let result = [];
        let at: XY = this.char;
        for(let dir of dirs) {
            let dest = Point.add(at, dir);
            result.push(new MovementCommand(this.gm, this.char, dest));
            at = dest;
        }

        return result;
    }

    public stepToward(who: Character, targetLoc: XY, ignoreChars: boolean): MovementCommand {
        let {x:cx, y:cy} = this.char.loc;
        let astar = new ROT.Path.AStar(
            targetLoc.x, targetLoc.y,
            this.gm.passableCallback(this.char.loc, ignoreChars));
        let path: XY[] = [];
        astar.compute(cx, cy, (x, y) => {
            if(cx != x || cy != y) {
                path.push({x,y});
            }
        });

        if(path.length >= 1) {
            let {x, y} = path[0];
            return new MovementCommand(this.gm, this.char, {x, y})
        }
        return null;
    }

    basicRanged(where: XY): Command {
        let ability = new BasicRangedAbility(this.gm, this.char);
        let cmd = new UseAbilityCommand(this.gm, this.char, ability, where);
        return cmd;
    }
}

export class CompoundCommand extends Command {
    public commands: Command[];
    constructor(...commands: Command[]) {
        super(commands[0]?.gm, commands[0]?.src);
        this.commands = commands.slice().reverse();
    }

    execute(): CommandResult {
        while(this.commands.length > 0) {
            let cmd = this.commands.pop();
            let result = cmd.execute();
            if(!result.success) {
                if (result.suggestedCommand) {
                    this.commands.push(result.suggestedCommand);
                } else {
                    return CommandResult.fail;
                }
            }
        }
        return CommandResult.pass;
    }
}

export class SitThereCommand extends Command {
    execute(): CommandResult {
        this.src.expend(Energy.MINOR);
        this.src.messages.msg("You wait");
        return CommandResult.pass;
    }
}

export class MovementCommand extends Command {

    constructor(
        gm: GameMaster,
        src: Character,
        protected moveTo: XY,
    ) {
        super(gm, src);
    }

    execute(): CommandResult {
        let passable = this.gm.canMoveInto(this.moveTo, Faction.EVERYBODY);
        if(!passable) return CommandResult.fail;

        let target = this.gm.map.tileAt(this.moveTo);
        if(target.character) {
            let result = new CommandResult(false);
            let ability = this.src.defaultMeleeAbility;
            let attack = new UseAbilityCommand(this.gm, this.src, ability, target);
            return result.suggest(attack);
        }

        this.doMove();

        this.src.expend(Energy.MOVE);
        return CommandResult.pass;
    }

    doMove() {
        let from = this.gm.map.tileAt(this.src);
        let target = this.gm.map.tileAt(this.moveTo);
        from.character = null;
        this.src.loc = this.moveTo;
        target.character = this.src;
        this.gm.recalculateFoV(this.src);
    }
}

export class UseAbilityCommand extends Command {
    constructor(
        gm: GameMaster,
        src: Character,
        protected what: Ability,
        protected where: XY,
    ) {
        super(gm, src);
    }

    execute(): CommandResult {
        //console.log("Used ability", this.what.name);
        let result = new CommandResult(this.what.invoke(this.where));
        if(result.success) {
            this.src.expend(Energy.STANDARD);
        }
        return result;
    }

}

export class EquipItemCommand extends Command {
    constructor(
        gm: GameMaster,
        src: Character,
        protected item: EquippableItem,
    ) {
        super(gm, src);
    }

    execute(): CommandResult {
        let where = this.item.slots[0];
        let alreadyThere = this.src.equipment[where];
        if(alreadyThere) {
            return new CommandResult().suggest(new CompoundCommand(
                new UnequipItemCommand(this.gm, this.src, where),
                this,
            ));
        }

        this.doEquip();
        this.src.messages.msg(`You equip the ${this.item.name}`);
        this.src.expend(Energy.STANDARD);
        return new CommandResult();
    }

    doEquip() {
        let where = this.item.slots[0];
        this.src.equipment[where] = this.item;
        this.item.onEquippedBy(this.src);
        this.src.removeItem(this.item);
    }
}

export class UnequipItemCommand extends Command {
    constructor(
        gm: GameMaster,
        src: Character,
        public slot: keyof EquipmentSlots,
    ) {
        super(gm, src);
    }

    execute(): CommandResult {
        let previous = this.src.equipment[this.slot];
        this.src.equipment[this.slot] = null;
        if(previous) {
            this.src.receiveItem(previous);
            this.src.messages.msg(`You unequip the ${previous.name}`);
        }
        return CommandResult.pass;
    }
}