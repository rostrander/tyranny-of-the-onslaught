import * as ROT from 'rot-js'

import {Character} from "./character";
import {Energy, exportJsonExcept, Faction, Point, XY} from "./common";
import {TileMap} from "./world";
import {Command} from "./commands";
import {PubSub} from "./pubsub";

export class GameMaster {

    characters: Character[];
    activeCharacter: Character;
    endOfTurn: PubSub;

    ready: Character[];

    public static fromJSON(template: Partial<GameMaster>) {
        let map = TileMap.fromJSON(template.map);
        let player = Character.fromJSON(template.player);
        let result = new GameMaster(map, player);

        for(let character of template.characters) {
            let newChar = Character.fromJSON(character);
            result.addCharacter(newChar);
        }
        return result;
    }

    constructor(public map: TileMap, public player?: Character) {
        this.characters = [];
        this.ready = [];
        this.endOfTurn = new PubSub();

        if(player) this.addCharacter(player);
    }

    addCharacter(character: Character, where?: XY): boolean {
        let addable = this.map.addCharacter(character, where);
        if(!addable) return false;

        this.characters.push(character);
        character.characterAddedBy(this);
        if(character.energy === Energy.ROLL_INITIATIVE) {
            let initiative = -ROT.RNG.getUniformInt(1, Energy.TURN);
            character.energy = initiative;
        }
        this.recalculateFoV(character);
        character.damageTaken.sub(msg => this.demonicBlood(msg.ar.value, character));
        return true;
    }

    public closestEnemyTo(src: Character): Character {
        let chosen = null;
        let closestDist = 99999;
        for(let char of this.characters) {
            if(char.faction !== src.faction &&
                src.canSee(char)
            ) {
                let dist = Point.distance(char.loc, src.loc);
                if(dist < closestDist) {
                    closestDist = dist;
                    chosen = char;
                }
            }
        }
        return chosen;
    }

    public canMoveInto(destination: XY, ignoreFaction=Faction.NOBODY): boolean {
        let tile = this.map.tileAt(destination);
        if(tile.isNullTile()) return false;
        if(!tile.passable) return false;
        if(tile.character && ignoreFaction !== Faction.EVERYBODY) {
            if(tile.character.faction !== ignoreFaction) return false;
        }
        return true;
    }

    public passableCallback(ignoreLocation: XY, ignoreChars = false):(x:number, y:number) => boolean {
        return (x: number,y: number) => {
            if (x == ignoreLocation.x && y == ignoreLocation.y) return true;
            let faction = ignoreChars ? Faction.EVERYBODY : Faction.NOBODY;
            return this.canMoveInto({x, y}, faction);
        };
    }

    recalculateFoV(center: Character) {
        center.visibleTiles = {};

        let fov = new ROT.FOV.PreciseShadowcasting((x: number, y: number) => {
            let tile = this.map.tileAt({x, y});
            return tile.passable;
        });

        /* output callback */
        fov.compute(center.x, center.y, center.sightDistance.value, (x, y, r, visibility) => {
            center.setSight({x,y}, visibility);
            if(center === this.player) {
                let tile = this.map.tileAt({x, y});
                tile.viewed = true;
            }
        });
    }

    turn() {
        this.supplyEnergy();
        let current: Character = this.determineActiveCharacter();
        while(current) {
            let took = this.takeTurn();
            if(!took) return;
            this.pruneDeadCharacters();
            current = this.determineActiveCharacter();
        }
    }

    supplyEnergy() {
        if(this.ready.length > 0) return;
        if(this.activeCharacter) return;

        let maxEnergy = -9999;
        // How much energy do we need to supply to get someone ready?
        for(let char of this.characters) {
            if(char.energy > maxEnergy) {
                if(char.isReady()) {
                    maxEnergy = 0;
                    break;
                }
                maxEnergy = char.energy;
            }
        }

        let energy = Math.abs(maxEnergy);
        // Supply that energy to everyone
        for(let char of this.characters) {
            char.energy += energy;
            if(char.isReady() && !this.ready.includes(char)) {
                this.ready.push(char);
            }
        }
    }

    determineActiveCharacter(): Character {
        if(this.activeCharacter) return this.activeCharacter;
        let result = this.ready.pop();
        if(result && !result.alive) return this.determineActiveCharacter();
        this.activeCharacter = result;
        return result;
    }

    takeTurn(): boolean {
        let who = this.activeCharacter;
        let cmd = who.nextCommand();
        if(!cmd) return false;
        let result = this.executeCommand(cmd);
        if(result) {
            this.endOfTurn.data(this.activeCharacter);
            this.activeCharacter = null;
        }
        return result;
    }

    pruneDeadCharacters() {
        let alive = [];
        for(let char of this.characters) {
            if(char.alive) {
                alive.push(char);
            } else {
                // TODO: Announce death via messaging system
                this.map.removeCharacter(char);
            }
        }
        this.characters = alive;
    }

    executeCommand(cmd: Command): boolean {
        let result = cmd.execute();
        if(result.success) return true;
        if(result.suggestedCommand) {
            return this.executeCommand(result.suggestedCommand);
        } else {
            return false;
        }
    }

    demonicBlood(amount: number, target: Character) {
        if(target.faction !== Faction.DEMON) return;
        this.player.demonicBlood += amount;
    }

    toJSON() {
        let result = exportJsonExcept(this,
            'activeCharacter',
            'ready',
        );
        // Player isn't included in 'characters' export because otherwise we
        // don't know which of the characters is the player on import
        result.characters = this.characters.filter((c) => c!==this.player);
        return result;
    }
}